#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <iomanip>

using namespace std;

template <typename T>
class Merge {
private:

    static void merge(vector<T>& vec, vector<T>& auxVec, int low, int mid, int high) {
        // precondition: orgVec[lo..mid] & orgVec[mid+1..high] are sorted
        for(int k = low; k <= high; k++)
            auxvec[k] = orgVec[k];

        // merge back to orgVec
        int i = low;
        int j = mid+1;
        for(int k = low; k <= high; k++) {
            // if one sub-array is out of elements, we take from the remaining one
            if(i > mid) orgVec[k] = auxVec[j++];
            else if (j > high) orgVec[k] = auxVec[i++];
            // take the smaller element into orgVec
            else if (aux[i] > aux[j]) orgVec[k] = aux[j++];
            else orgVec[k] = aux[i++];
        }
    }

    static void sort(vector<T>& orgVec, vector<T>& auxVec, int low, int high)  {
        if(high <= low) return;
        int mid = low + (high - low) / 2;
        sort(orgVec, auxVec, lo, mid);
        sort(orgVec, auxVec, mid+1, high);
        merge(orgeVec,auxVec, low, mid, high);
    }

public:

    static void sort(vector<T>& orgVec) {
        vector<T> auxVec(orgVec.size());
        sort(orgVec, auxVec, 0, orgVec.size() - 1);
    }

};

// Merge sort
int main(int argc, char** argv) {

    vector<int> arr(stoi(argv[1]));
    iota(arr.begin(), arr.end(), 0);

    cout << "Before sorting: ";
    random_shuffle(arr.begin(),arr.end());
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    Merge<int>::sort(arr);
    // mergesort function
    cout << "After sorting: ";
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    return 0;

}
