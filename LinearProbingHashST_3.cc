#include <iostream>
#include <cmath>
#include <cstring>
#include <sstream>
#include "Queue.h"

using namespace std;

const int PRIMES[] = {
    1, 1, 3, 7, 13, 31, 61, 127, 251, 509, 1021, 2039, 4093, 8191, 16381,
    32749, 65521, 131071, 262139, 524287, 1048573, 2097143, 4194301,
    8388593, 16777213, 33554393, 67108859, 134217689, 268435399,
    536870909, 1073741789, 2147483647
};

template <typename Key, typename Value>
class LinearProbingHashST {
private:
    int hash(Key key) {
        int hashCode = std::hash<Key>{}(key) & INT_MAX;
        hashCode = (logM < 26) ? hashCode % PRIMES[logM+5] : hashCode;
        return hashCode % M;
    }

    void resize(int cap) {
        Key** keyTemp = new Key*[cap];
        Value** valTemp = new Value*[cap];
        memset(keyTemp, 0x0, sizeof(Key*)*cap);
        memset(valTemp, 0x0, sizeof(Value*)*cap);
        int oldM = M;
        M = cap;
        logM = (int) log(M) / log(2);
        if (keys) {
            for (int i = 0; i < oldM; i++) {
                if (keys[i]) {
                    int j;
                    for (j = hash(*keys[i]); keyTemp[j]; j = (j+1)%M);
                    keyTemp[j] = keys[i];
                    valTemp[j] = vals[i];
                }
            }
        }
        keys = keyTemp; vals = valTemp;
        keyTemp = 0x0; valTemp = 0x0;
        delete keyTemp; delete valTemp;
    }

    int getIndex(Key key) {
        int i = hash(key);
        while (keys[i]) {
            if (**keys[i] == key) return i;
            i = (i+1)%M;
        }
        return -1;
    }
    
    Key** keys = 0x0;
    Value** vals = 0x0;
    int M = 17;
    int N = 0;
    int logM;

public:
    friend ostream& operator << (ostream& os, const LinearProbingHashST& st) {
        for (int i = 0; i < st.M; i++)
            if (st.keys[i])
                os << to_string(i) << ". Key: " << *st.keys[i] << ". Value: " << *st.vals[i] << "\n";
        return os;
    }

    LinearProbingHashST(): LinearProbingHashST(17) {}

    LinearProbingHashST(int cap) { resize(cap); }

    void put(Key key, Value val) {
        if (N >= M /2) resize(M*2);
        int i;
        for (i = hash(key); keys[i]; i = (i+1)%M) {
            if (*keys[i] == key) {
                delete vals[i];
                vals[i] = new Value(val);
                return;
            }
        }
        keys[i] = new Key(key);
        vals[i] = new Value(val);
        N++;
    }

    void deleteKey(Key key) {
        if (!contains(key)) return;
        int i = hash(key);
        while (keys[i]) {
            if (*keys[i] == key) break;
            i = (i+1)%M;
        }
        keys[i] = 0x0;
        vals[i] = 0x0;
        i = (i+1)%M;
        while (keys[i]) {
            Key keyToRedo = **keys[i];
            Value valToRedo = **vals[i];
            keys[i] = 0x0;
            vals[i] = 0x0;
            N--;
            put(keyToRedo, valToRedo);
            i = (i+1)%M;
        }
        N--;
        if (N > 0 && N == M/8) resize(M/2);
    }

    bool contains(Key key) { return getIndex(key) != -1; }

    Value get(Key key) { return getIndex(key) == -1 ? NULL : vals[getIndex(key)]; }

    Queue<Key> getKeys() {
        Queue<Key> queue;
        for (int i = 0; i < M; i++)
            if (keys[i]) 
                queue.enqueue(**keys[i]);
        return queue;
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }

};

int main() {
    int length;
    cin >> length;
    char c;
    LinearProbingHashST<char,int> st;
    for (int i = 0; i < length; i++) {
        cin >> c;
        st.put(c,i);
    }
    cout << st.toString() << endl;
}
