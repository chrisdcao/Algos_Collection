#include <iostream>
#include <vector>
#include "Random.h"
#include "ContainerUtility.h"
#include "Stack.h"

using namespace std;

/* MaxHeap: parent >= child nodes */
template <typename T>
class MaxPQ {
private:

    vector<T> pq;
    int N = 0;

    void sink(int k) {
        while (2 * k < N) {
            int j = 2 * k;
            /* we search for the boundary/stopping point (at which: pq[j-1] >= sinking_value >= pq[j+1])
             * only fitting the value there ensures the structure of the heap, until then we keep incrementing j */
            if (j < N && pq[j] < pq[j+1]) j++;
            /* if the precessor is already larger, we don't have to do any swapping */
            if (pq[k] >= pq[j]) break;
            swap(pq[j], pq[k]);
            k = j;
        }
    }

    void swim(int k) {
        while (pq[k/2] < pq[k] && k > 1) {
            swap(pq[k/2], pq[k]);
            k = k/2;
        }
    }

public:

    friend ostream& operator <<(ostream& os, MaxPQ<T> maxPQ) {
        while (!maxPQ.isEmpty())
            os << maxPQ.delMax() << " ";
        return os;
    }

    MaxPQ() = default;

    MaxPQ(int maxN): pq(maxN+1) {}

    virtual ~MaxPQ() { /* not implemented */ }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insert(T item) {
        pq[++N] = item;
        swim(N);
    }

    T getMax() { return pq[1]; }

    T delMax() {
        if (N == 0) {
            cout << "Underflow. Returning default value for the type: " << endl;
            return T();
        }
        T max = pq[1];
        swap(pq[1], pq[N--]);
        pq[N+1] = NULL;
        sink(1);
        return max;
    }

};

int main(int argc, char** argv) {

    int M = stoi(argv[1]);
    MaxPQ<int> pq(M + 1);
    int x;
    while (cin >> x) {
        pq.insert(x);
        if (pq.size() > M)
            pq.delMax();    
    }
    Stack<int> stack;
    while (!pq.isEmpty()) stack.push(pq.delMax());
    for (auto t : stack) cout << t << endl;

    return 0;

}
