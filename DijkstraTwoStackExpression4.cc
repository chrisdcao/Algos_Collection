#include <iostream>
#include <string>
#include <stack>
#include <math.h>
using namespace std;

int main() {
    stack<string> ops;
    stack<double> vals;
    string s;
    while (cin >> s) {
        if (s == "(");
        else if (s == "+") ops.push(s);
        else if (s == "-") ops.push(s);
        else if (s == "*") ops.push(s);
        else if (s == "/") ops.push(s);
        else if (s == "sqrt") ops.push(s);
        else if (s == ")") {
            string op = ops.top();
            ops.pop();
            double val = vals.top();
            vals.pop();
            if (op == "+") {
                val = vals.top() + val;
                vals.pop();
            }
            else if (op == "-") {
                val = vals.top() - val;
                vals.pop();
            }
            else if (op == "*") {
                val = vals.top() * val;
                vals.pop();
            }
            else if (op == "/") {
                val = vals.top() / val;
                vals.pop();
            }
            else if (op == "sqrt") val = sqrt(val);
            vals.push(val);
        }
        else {      // it has to be a number
                    // and we will push to vals stack
            double get = stod(s, 0);
            vals.push(get);
        }
    }
    cout << vals.top() << endl;
}
