#include <iostream>
#include <random>
#include <vector>
#include <algorithm>
#include "Random.h"

using namespace std;

template <typename T>
class Quick3Way {
private:

    static void sort(vector<T>& a, int lo, int hi) {
        if (hi <= lo) return;
        int left = lo, i = lo+1, right = hi;
        T pivot = a[lo];
        while (i <= right) {
            if (a[i] < pivot) swap(a[i++], a[left++]);
            else if (a[i] > pivot) swap(a[i], a[right--]);
            else i++;
        }
        sort(a, lo, left-1);
        sort(a, right+1, hi);
    }

public:

    static void sort(vector<T>& a) {
        random_shuffle(a.begin(), a.end());
        sort(a, 0, a.size()-1);
    }

};

int main(int argc, char** argv) {

    vector<int> vec;
    int N = stoi(argv[1]);
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));

    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    Quick3Way<int>::sort(vec);

    cout << "The array after sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    return 0;

}

