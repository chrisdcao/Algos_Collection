#include <iostream>
using namespace std;

int RBinarySearch(int key, int a[], int lo, int hi) {
    if (lo > hi) return -1;
    int mid = lo + (hi - lo) / 2;
    if (key < a[mid]) return RBinarySearch(key, a, lo, mid - 1);
    else if (key > a[mid]) return RBinarySearch(key, a, mid + 1, hi);
    else return mid;
}

int main(int argc, char **argv) {
    int n = 4;
    int a[] = { 1, 2, 3, 4, 8, 9 };
    int lo = 0;
    int hi = sizeof(a) / sizeof(a[0]) - 1;
    cout << RBinarySearch(n, a, lo, hi) << endl;

    n = 7;
    lo = 0;
    hi = sizeof(a) / sizeof(a[0]) - 1;
    cout << RBinarySearch(n, a, lo, hi) << endl;

    n = 1;
    lo = 0;
    hi = sizeof(a) / sizeof(a[0]) - 1;
    cout << RBinarySearch(n, a, lo, hi) << endl;

    n = 9;
    lo = 0;
    hi = sizeof(a) / sizeof(a[0]) - 1;
    cout << RBinarySearch(n, a, lo, hi) << endl;
    return 0;
}
