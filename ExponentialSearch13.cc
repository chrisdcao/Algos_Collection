#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <numeric>

using namespace std;

template <typename T>
class ExponentialSearch {
private:
    static int BinarySearch(T key, int lo, int hi, vector<T>& vec) {
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
            if (key > vec[mid])      lo = mid + 1;
            else if (key < vec[mid]) hi = mid - 1;
            else                     return mid;
        }
        return -1;
    }

public:
    static int indexOf(T key, vector<T>& vec) {
        if(vec[0] == key ) return 0;

        int i = 1;
        while (vec[i] < key && i < vec.size()) i *= 2; 

        int lo = i / 2;
        int hi = min(i, (int) vec.size() -1);

        return BinarySearch(key, lo, hi, vec);
    }
};

int main() {
    vector<int> vec(10); 
    iota(vec.begin(), vec.end(), 0);

    cout << ExponentialSearch<int>::indexOf(7, vec) << endl;
    cout << ExponentialSearch<int>::indexOf(0, vec) << endl;
    cout << ExponentialSearch<int>::indexOf(9, vec) << endl;
    cout << ExponentialSearch<int>::indexOf(19, vec) << endl;

    return 0;
}
