#include <iostream>
#include <vector>
#include <memory>
#include "Queue.h"
#include <sstream>

using namespace std;

template <typename Key, typename Value>
class SeparateChainingHashST {
private:
  class Node {
  public:
    Key key;
    Value val;
    Node* next = NULL;
    int index = -1;

    friend ostream& operator << (ostream& os, const Node& h) {
      os << "Key: " << h.key << ". Value: " << h.val << ". Next: ";
      os << (h.next ? h.next->key : "null");
      return os;
    }

    string toString() {
      ostringstream oss;
      oss << *this;
      return oss.str();
    }
  };

  Node* newNode(Key key, Value val) {
    allNodes.push_back(make_unique<Node>());
    allNodes.back()->index = allNodes.size()-1;
    allNodes.back()->key = key;
    allNodes.back()->val = val;
    return allNodes.back().get();
  }

  void freeNode(Node* x) {
    if (x->index < allNodes.size()-1) {
      allNodes[x->index].swap(allNodes.back());
      allNodes[x->index]->index = x->index;
    }
    allNodes.pop_back();
  }

  int hash(Key key) {
    return (std::hash<Key>{}(key) & 0x7fffffff) % M;
  }

  // a vector at each indices of whose is a linked list 
  vector<Node*> st;
  vector<unique_ptr<Node>> allNodes;
  int M = 17;
  int N = 0;

public:
  // we will define the constructor taking in input first
  SeparateChainingHashST(int M): M(M)  { 
    st.resize(M); // built-in resize funct ion of vector
  }
  // then the default constructor
  SeparateChainingHashST(): SeparateChainingHashST(17) {}

  void put(Key key, Value val) {
    Node* ptr = st[hash(key)];
    if (ptr) {
      // if the bucket has the same key we replace the value the return
      if (ptr->key == key)  {
        ptr->val = val;
        return;
      }
      // else we traverse within the bucket
      while (ptr->next) {
        if (ptr->key == key) {
          ptr->val = val;
          return;
        }
        ptr = ptr->next;
      }
      // if bucket doesn't have the key we insert the new key in
      // since the stopping condit ion is ptr->next == NULL, we insert at 'ptr->next' in the bucket
      ptr->next = newNode(key, val);
    } else { // if ptr is NULL from the start, we insert the right away
      st[hash(key)] = newNode(key, val);
    }
    N++;
  }

  void deleteKey(Key key) {
    Node* ptr = st[hash(key)];
    // this is to indicate if it's the only member at that index (so we just remove by sett ing st[index] = NULL)
    Node* ptrPrev = NULL;
    // else generally, we remove the node within the collision bucket like normal dis-linking in a linked list
    while (ptr) {
      if (key == ptr->key) {
        if (ptrPrev) // if ptrPrev is not NULL (we have the node addr before this to-delete node)
          ptrPrev->next = ptr->next; // disconnect the link like linked list
        else {
          if (ptr->next) st[hash(key)] = ptr->next;
          else           st[hash(key)] = NULL;
        }
        N--;
        return;
      }
      ptrPrev=  ptr;
      ptr = ptr->next;
    }
  }

  Value get(Key key) {
    Node* ptr = st[hash(key)];
    while (ptr) {
      if (ptr->key == key) 
        return ptr->val;
      ptr = ptr->next;
    }
    return NULL;
  }

  bool contains(Key key) {
    return get(key) != NULL;
  }

  Queue<Key> keys() {
    Queue<Key> queue;
    for (int i= 0;i< M; i++) {
      Node* ptr = st[i];
      while (ptr) {
        queue.enqueue(ptr->key);
        ptr = ptr->next;
      }
    }
    return queue;
  }

  string toString() {
    string str = "\n";
    for (int i= 0;i< M; i++) {
      Node* ptr = st[i];
      while (ptr) {
        str += to_string(i) + " " + ptr->toString() + "\n";
        ptr = ptr->next;
      }
    }
    return str;
  }

};

int main() {
  string keyArr[] = { "E", "A", "S", "Y", "Q", "U", "T", "I", "O", "N" };
  string valueArr[] = { "E", "A", "S", "Y", "Q", "U", "T", "I", "O", "N" };
  SeparateChainingHashST< string, string > hashST( 17 );

  for ( int i= 0;i< sizeof(keyArr)/sizeof(string); ++i )
      hashST.put( keyArr[i], valueArr[i] );

  cout << hashST.toString() << endl;

  Queue< string > keysQueue = hashST.keys();
  while ( ! keysQueue.isEmpty() ) 
    cout << keysQueue.dequeue() << endl;
  cout << endl;

  return 0;
}
