#include <iostream>
#include <vector>
#include <numeric>
#include <iomanip>
#include "Random.h"

using namespace std;

template <typename Key, typename Value>
class BinarySearchST {
private:

    vector<Key> key_vec;
    vector<Value> val_vec;
    int N = 0;

public:

    BinarySearchST() = default;

    int size() { return N; }

    bool is_empty() { return N == 0; }

    bool contains(Key key) {
        return (is_empty()) ? false : (key_vec[rank(key)] == key);
    }

    void put(Key key, Value val) {
        if (!is_empty()) {
            int pos = rank(key);
            if (key_vec[pos] == key) {
                val_vec[pos] = val; return;
            } else {
                key_vec.insert(key_vec.begin() + pos, key);
                val_vec.insert(val_vec.begin() + pos, val);
            }
        } else {
            key_vec.push_back(key);
            val_vec.push_back(val);
        }
        N++;
    }

    Value select(Key key) {
        int pos = rank(key);
        return (key_vec[pos] == key) ? val_vec[pos] : NULL;
    }

    vector<Key>& keys() { return key_vec; }

    // largest smaller
    Key floor(Key key) {
        if (key < min()) throw logic_error("floor(): No floor (key < min())");
        int pos = rank(key);
        return (key_vec[pos] == key) ? key_vec[pos] : key_vec[pos-1];
    }

    Key ceiling(Key key) {
        if (key > max()) throw logic_error("ceiling(): No ceiling (key > max())");
        return key_vec[rank(key)];
    }

    Key min() { return key_vec[0]; }
    Key max() { return key_vec[N-1]; }

    void eraseMax() { erase(max()); }

    void eraseMin() { erase(min()); }

    void erase(Key key) {
        int pos = rank(key);
        if (key_vec[pos] != key) throw runtime_error("erase(): There's no such key");
        else {
            key_vec.erase(key_vec.begin()+pos);
            val_vec.erase(val_vec.begin()+pos);
            N--;
        }
    }

    int rank(Key key) {
        if (is_empty()) throw out_of_range("rank(): The table is empty()");
        int lo = 0, hi = key_vec.size() - 1;
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
            if      (key > key_vec[mid]) lo = mid + 1;
            else if (key < key_vec[mid]) hi = mid - 1;
            else                         return mid;
        }
        return lo;
    }

};

int main(int argc, char** argv) {

    BinarySearchST<int, int> binary_search_st;
    for (int i = 0; i < 20; i++)
        binary_search_st.put(random_uniform_distribution(40, 100), i);

    for (auto key : binary_search_st.keys())
        cout << key << endl;

}
