#include <iostream>
#include <vector>
#include <string>
#include <memory>
using namespace std;

template <typename T>
class Bag {
private:
    class Node {
    public:
        T item;
        Node* next;
        int index = -1;
    };
    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }
    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }
    vector<unique_ptr<Node>> allNodes;
    Node* head;
    Node* tail;
    int N;
    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* initLoc) {
            current = initLoc;
        }
        Iterator& operator++() {
            current = current->next;
            return *this;
        }
        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }
        T operator*() {
            return this->current->item;
        }
    };
public:
    Bag() {
        N = 0;
        head = NULL;
        tail = NULL;
    }

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void addItem(T item) {
        Node* temp = newNode();
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
            tail = temp;
        } else {
            temp->next = head;
            head = temp;
        }
        ++N;
    }

    Iterator begin() { return Iterator(head); }
    Iterator end() { return Iterator(tail->next); }
};

int main() {
    Bag<string> stringBag;
    cout << "Expected outcome: Bag is empty" << endl;
    cout << "Real outcome: ";
    if (stringBag.isEmpty())
        cout << "Bag is empty" << endl;
    else 
        cout << "Bag is occupied" << endl;
    stringBag.addItem("thinh");
    stringBag.addItem("phong");
    stringBag.addItem("cuong");
    cout << endl;
    cout << "Expected outcome: 3" << endl;
    cout << "Real outcome: ";
    cout << stringBag.size() << endl;
    cout << endl;
    cout << "Expected outcome: Bag is occupied" << endl;
    cout << "Real outcome: ";
    if (stringBag.isEmpty())
        cout << "Bag is empty" << endl;
    else 
        cout << "Bag is occupied" << endl;
    cout << endl;
    cout << "Expected outcome: cuong phong thinh" << endl;
    cout << "Real outcome: ";
    for (auto s : stringBag)
        cout << s << " ";
    cout << endl;
}
