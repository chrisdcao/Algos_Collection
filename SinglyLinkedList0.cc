#include <iostream>
#include <string>
#include <sstream>
#include <memory>
#include <vector>

using namespace std;

template <typename T>
class SinglyLinkedList {
private:
    class Node {
    public:
        T item;
        Node* next = NULL;
        int index = -1;
    };

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    vector<unique_ptr<Node>> allNodes;

    Node* head;
    int N;

public:
    friend ostream& operator << (ostream& os, const SinglyLinkedList& list) {
        auto traverse = list.head;
        while (traverse != NULL) {
            os << traverse->item << " ";
            traverse = traverse->next;
        }
        os << endl;
        return os;
    }

    SinglyLinkedList(): N(0), head(NULL) {}

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void insert(T item) {
        Node* temp = newNode();
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
        } else {
            temp->next = head;
            head = temp;
        }
        N++;
    }

    void insertAfter(int index, T item) {
        if (index < 0) {
            cout << "The index cannot be negative" << endl;
            return;
        } else {
            if (index > N) {
                cout << "The index is out of bound" << endl;
                return;
            } else {
                Node* traverse = head;
                for (int i = 0; i < index; i++)
                    traverse = traverse->next;
                Node* temp = newNode();
                temp->item = item;
                temp->next = traverse->next;
                traverse->next = temp;
            }
        }
        N++;

    }

    void insertAtEnd(T item) {
        return insertAfter(size() - 1, item);
    }

    Node* reverseIteratively(Node* head) {
        if (N <= 1) return head;
        else {
            Node* prev = NULL;
            Node* current = head;
            Node* next = head->next;
            while (next !=  NULL) {
                current->next = prev;
                prev = current;
                current = next;
                next = next->next;
            }
            current->next = prev;
            return current;
        }
    }

    void removeAtBegin() {
        Node* oldHead = head;
        head = head->next; 
        freeNode(oldHead);
        --N;
    }

    Node* reverseRecursively(Node* head) {
        if (head == NULL)  return NULL;
        if (head->next == NULL) return head;
        Node* second = head->next;
        Node* newHead = reverseRecursively(second);       // we reverse all the remaning node (from bottom-to-top order) in all these recursions
        second->next = head;        
        head->next = NULL;                  // then we complete the reverse by reversing the first 2 nodes
        return newHead;                        // Node rest, after the recursion, has travelled to the tail of the 'old' list, and now become the head of the new-reversed list
    }

    void reverse(bool isRecursive = true) {
        if (isRecursive)
            head = reverseRecursively(head);
        else
            head = reverseIteratively(head);
    }

    string toString() const {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }
};

int main(int argc, char** argv) {
    SinglyLinkedList<string> stringList;
    stringList.insert("phong");
    stringList.insert("thinh");
    stringList.insert("cuong");
    stringList.insert("khanh");

    cout << "Expected outcome: khanh cuong thinh phong" << endl;
    cout << "Real outcome: ";
    cout << stringList;
    cout << endl;

    cout << "Expected outcome: khanh cuong thinh phong" << endl;
    cout << "Real outcome: ";
    cout << stringList.toString();
    cout << endl;

    stringList.reverse();
    cout << "Expected outcome: phong thinh cuong khanh" << endl;
    cout << "Real outcome: ";
    cout << stringList;
    cout << endl;

    stringList.reverse(false);
    cout << "Expected outcome: khanh cuong thinh phong" << endl;
    cout << "Real outcome: ";
    cout << stringList;
    cout << endl;

    return 0;
}
