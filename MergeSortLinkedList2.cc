#include <iostream>
#include "SinglyLinkedList.h"
#include "Random.h"

using namespace std;

#define List SinglyLinkedList

template <typename T>
class Merge {
    using NodePtr = SinglyLinkedList<int>::Node*;
private:

    /* this one is to after splitting, we sort the elements back in sorted order */
    static void MergeSort(NodePtr* headRef) {
        NodePtr head = *headRef, a, b;

        if (head == NULL || (head->next == NULL)) return;

        FrontBackSplit(head, &a, &b);

        MergeSort(&a);
        MergeSort(&b);

        /* assign the current head of the array to the new head of the sorted array */
        *headRef = SortedMerge(a,b);
    }

    static NodePtr SortedMerge(NodePtr a, NodePtr b) {
        NodePtr result = NULL;
        if (a == NULL) return b;
        else if (b == NULL) return a;

        if (a->item <= b->item) {
            result = a;
            result->next = SortedMerge(a->next, b);
        } else {
            result = b;
            result->next = SortedMerge(a, b->next);
        }
        return result;
    }

    static void FrontBackSplit(NodePtr source, NodePtr* frontRef, NodePtr* backRef) {
        NodePtr fast;
        NodePtr slow;

        slow = fast = source;

        while (fast != NULL) {
            fast = fast->next;
            if (fast != NULL) {
                slow = slow->next;
                fast = fast->next;
            }
        }

        *frontRef = source;
        *backRef = slow->next;
        slow->next = NULL;
    }

public:

    static void sort(LinkedList<T>& org) {
        NodePtr* headRef = &org.head;
        MergeSort(headRef);
    }

};

int main(int argc, char** argv) {

    int N = stoi(argvp[1);
    List<int> intList;
    for (int i = 0; i < N; i++)
        intList.insert(randomUniformDistribution(0, N-1));

    cout << "The array before sorting is: ";
    cout << intList << endl;
    
    Merge<int>::sort(intList);

    cout << "The array after sorting is: ";
    cout << intList << endl;

    return 0;

}
