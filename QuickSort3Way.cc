#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include "Random.h"

using namespace std;

template <typename T>
class Quick3Way {
private:
    
    /* this one keeps going until hi <= low */
    static void sort(vector<T>& a, int lo, int hi) {
        if (hi <= lo) return;
        int lt = lo, i = lo+1, gt = hi;
        T v = a[lo];
        cout << "The pivot is: " << v << endl;
        /* this is the partitioning part, with the method of partitioning similar to Chines video on partitioning, except that it uses 2 pointers to keep track of the value of i, instead of only having one pointer for the operation. This enables to cut cost in half comppared to one pointer
         * we choose the first point as the pivot, we compare (active pointer-1 index after the first and react pointer-the last) with this pivot
         * once we see an unsuitable element, the active pointer is swapped with the react pointer, then we move react pointer backward 1, and active pointer (after swapping) stay in the same place so we can take this NEWLY swapped value and keep comparing it with the passive pointer, until the traverse is over */
        while (i <= gt) {
            if (a[i] < v)       swap(a[lt++], a[i++]);
            /* we just know that it has to be on the right side so we switch it with an unknown/random item on the right side. This swapping does NOT necessarily put the a[i] into its final place, but the purpose of partition is to divided into 2 correct sides rather than specifically put each element in place right away */
            else if (a[i] > v)  swap(a[i], a[gt--]);
            else                i++;
        }
        cout << "The array after partitioning: ";
        for (auto s : a)
            cout << s << " ";
        cout << endl;
        /* then we recursively use this method of partitioning with divide and conquer mindset (the array keeps being broken down into smaller in each recursive loop)
         * we use lt-1 and gt+1 as boundary as any elements in between have already been sorted, so we only have to run within these bounds */
        sort(a, lo, lt-1);
        sort(a, gt+1, hi);
    }

public:

    static void sort(vector<T>& a) {
        random_shuffle(a.begin(), a.end());
        sort(a, 0, a.size()-1);
    }

};

int main(int argc, char** argv) {

    vector<int> vec;
    int N = stoi(argv[1]);
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));

    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    Quick3Way<int>::sort(vec);

    cout << "The array after sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    return 0;

}

