#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

/* MinHeap: Parent < Child */
template <typename T> 
class MinPQ {
private:

    vector<T> pq;

    int N = 0;

    void sink(int k) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j] > pq[j+1]) j++;
            if (pq[k] <= pq[j]) break;
            swap(pq[k], pq[j]);
            k = j; 
        }
    }

    void swim(int k) {
        while (k > 1 && pq[k/2] > pq[k]) {
            swap(pq[k/2], pq[k]);
            k = k/2;
        }
    }

public:

    MinPQ(int maxN): pq(maxN+1) {}

    virtual ~MinPQ() {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insert(T item) {
        pq[++N] = item;
        swim(N);
    }
    
    T getMin() { return pq[1]; }

    T delMin() {
        T item = pq[1];
        swap(pq[1], pq[N--]);
        sink(1);
        return item;
    }

    friend ostream& operator << (ostream& os, MinPQ<T> minPQ) {
        while (!minPQ.isEmpty())
            os << minPQ.delMin() << " ";
        return os;
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);

    MinPQ<int> minPQ(N);

    for (int i = 0; i < N; i++)
        minPQ.insert(randomUniformDistribution(0, N-1));

    cout << minPQ << endl;

    return 0;

}
