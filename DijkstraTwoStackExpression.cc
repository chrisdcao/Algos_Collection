#include <iostream>
#include <stack>
#include <string>
#include <math.h>
using namespace std;

int main() {
    stack<string> ops;
    stack<double> vals;
    string s;
    while (cin >> s) {              // only work for separated string input, meaning that without 'space' or 'enter', the program will take it as 'one string - one input' and will NOT perform any computation
        if (s == "(");
        else if (s == "+" ) ops.push(s);
        else if (s == "-" ) ops.push(s);
        else if (s == "*" ) ops.push(s);
        else if (s == "/" ) ops.push(s);
        else if (s == "sqrt" ) ops.push(s);
        else if (s == ")" ) {       // only work as many operations as the number of ")", therefore, bao nhiêu calculation phai có bây nhiêu ")"
            string op = ops.top();
            ops.pop();
            double v = vals.top();
            vals.pop();
            if (op == "+") {
                v = vals.top() + v;
                vals.pop();
            }
            else if (op == "-") {
                v = vals.top() - v;
                vals.pop();
            }
            else if (op == "*") {
                v = vals.top() * v;
                vals.pop();
            }
            else if (op == "/") {
                v = vals.top() / v;
                vals.pop();
            }
            else if (op == "sqrt") v = sqrt(v);
            vals.push(v);
        }
        else {
            double get = stod(s, 0);
            vals.push(get);
        }
    }
    cout << vals.top() << endl;
}
