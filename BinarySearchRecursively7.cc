#include <iostream>
using namespace std;

int BinarySearch(int key, int a[], int lo, int hi) {
    if (lo > hi) return -1;
    int mid = lo + (hi - lo) / 2;
    if (key < a[mid]) return BinarySearch(key, a, lo, mid - 1);
    else if (key > a[mid]) return BinarySearch(key, a, mid + 1, hi);
    else return mid;
}

int main () {
    int a[] = {1, 2, 2, 5, 6, 6, 8, 9, 12};
    int lo = 0;
    int hi = sizeof(a) / sizeof(a[0]);
    cout << BinarySearch(5, a, lo, hi) << endl;
    cout << BinarySearch(1, a, lo, hi) << endl;
    cout << BinarySearch(12, a, lo, hi) << endl;
    cout << BinarySearch(17, a, lo, hi) << endl;
}
