#include <iostream>
#include <iomanip>
#include <numeric>
#include <memory>
#include <vector>
#include "Queue.h"
#include "Stack.h"
#include "Random.h"

using namespace std;

template < typename Key, typename Value >
class BinarySearchTree {
private:

    class Node {
    public:
        Key key;
        Value val;
        Node* left;
        Node* right;
        int index = -1;
        int N = 1;
    };

    vector< unique_ptr< Node > > allNodes;
    Node* root = NULL;
    int maxHeight = 0;

    Node* newNode( Key key, Value val, int N ) {
        allNodes.push_back( make_unique< Node >() );
        allNodes.back()->index = allNodes.size() - 1;
        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        return allNodes.back().get();
    }

    void freeNode( Node* x ) {
        if ( x->index < allNodes.size() - 1 ) {
            allNodes[ x->index ].swap( allNodes.back() );
            allNodes[ x->index ]->index = x->index;
        }
        allNodes.pop_back();
    }

    int height( Node* x, int count ) {
        if ( !x ) return NULL;
        int leftHeight = height( x-left, count + 1 );
        int rightHeight = height( x->right, count + 1 );
        return ( rightHeight < leftHeight ) ? leftHeight : rightHeight;
    }

    Node* eraseMin( Node* x ) {
        if ( !x ) return NULL;
        if ( x->key == min() ) {
            x = x->right; 
            x->N -= 1;
            return x;
        } else {
            Node* xFast = x->left;
            while ( xFast->left ) {
                xFast = xFast->left;
                x->N -= 1;
                x = x->left;
            }
            x->N -= 1;
            x->left = xFast->right;
        }
        return x->left;
    }

    Node* eraseMax( Node* x ) {
        if ( !x ) return NULL;
        if ( size( x ) == 1 || x->key == max() ) {
            x = x->left; 
            x->N -= 1;
            return x;
        } else {
            Node* xFast = x->right;
            while ( xFast->right ) {
                xFast = xFast->right;
                x->N -= 1;
                x = x->right;
            }
            x->N -= 1;
            x->right = xFast->left;
        }
        return x->right;
    }

    Node* min( Node* x ) {
        if ( !x ) return NULL;
        while ( x->left ) {   x = x->left;   }
        return x;
    }

    Node* max( Node* x ) {
        if ( !x ) return NULL;
        while ( x->right ) {   x = x->right;   }
        return x;
    }

public:

    int size( Node* x ) { return ( !x ) ? 0 : x->N; }

    int size() { return size( root ); }

    int height() { return height( root, 0 ); }

    bool isEmpty() { return size( root ) == 0; }

    void put( Key key, Value val ) {
        int currentHeight = 0;
        if ( size() == 0 ) root = newNode( key, val, 0 );
        else {
            Node* x = root;
            string nextLink = "none";
            while ( x ) {
                if ( x->key > key ) {
                    nextLink = "left";
                    currentHeight++;
                    if ( !x->left ) break;
                    x = x->left;
                }
                else if ( x->key < key ) {
                    nextLink = "right";
                    currentHeight++;
                    if ( !x->right ) break;
                    x = x->right;
                }
                else { x->val = val; return; }
            }
            if      ( nextLink == "left" )  x->left = newNode( key, val, 0 );
            else if ( nextLink == "right" ) x->right = newNode( key, val, 0 );

            maxHeight = currentHeight;

            Node* t = root;
            while ( t ) {
                if ( t->key > key ) {
                    t = t->left;
                    if ( !t ) break; t->N += 1;
                } else if ( t->key < key ) {
                    t = t->right;
                    if ( !t ) break; t->N += 1;
                } else break;
            }
        }
        // update the root
        root->N = size( root->left ) + size( root->right ) + 1;
    }

    Key min() { return min( root )->key; }

    Key max() { return max( root )->key; }

    void eraseMin() { eraseMin( root ); }

    void eraseMax() { eraseMax( root ); }

    void erase( Key key ) {
        if ( root == NULL ) throw out_of_range( "The tree is currently empty!" );
        // if key not exist in the tree, return
        if ( select(rank(key)) != key ) return;
        Node* x = root;
        Node* prevNode = NULL;
        string explore = "none";
        while( x ) {
            if ( x->key > key ) {
                prevNode = x; prevNode->N -= 1;
                x = x->left;
                explore = "left";
            } else if ( x->key < key ) {
                prevNode = x; prevNode->N -= 1;
                x = x->right;
                explore = "right";
            } else {
                if ( !x->right ) {   
                    if ( x == root )          { root = root->left; break; } 
                    if ( explore == "right" ) prevNode->right = prevNode->right->left;
                    else                      prevNode->left = prevNode->left->left;
                    break;
                } else if ( !x->left ) {   
                    if ( x == root )          { root = root->right; break; }
                    if ( explore == "right" ) prevNode->right = prevNode->right->left;
                    else                      prevNode->left = prevNode->left->left;
                    break;   
                }
                Node* temp = x;
                x = min( temp->right );
                x->right = eraseMin( temp->right );
                x->left = temp->left;
            }
        }
    }

    Value get( Key key ) {
        Node* x = root;
        while ( x ) {
            if      ( x->key > key ) x = x->left;
            else if ( x->key < key ) x = x->right;
            else                     return x->val;
        }
        return NULL;
    }

    Key select( int k ) {
        Node* x = root;
        int t = size( x->left );
        while ( x ) {
            if ( t > k ) {   
                x = x->left;
            } else if ( t < k ) {
                x = x->right;
                k = k-t-1;
            }
            else return x->key;

            if ( !x ) break;
            t = size( x->left );
        }
        return NULL;
    }

    // largest smaller
    Key floor( Key key ) {
        if ( root == NULL ) return NULL;
        Node* x = root;
        Node* currentSatisfied = NULL;
        while ( x ) {
            if ( x->key > key ) x = x->left;
            else if ( x->key == key ) return x->key;
            else {
                if ( currentSatisfied == NULL || currentSatisfied->key < x->key ) 
                    currentSatisfied = x;
                if ( !x->right ) return x->key;
                else x = x->right;
            }
        }
        return ( currentSatisfied == NULL ) ? NULL : currentSatisfied->key;
    }

    // smallest larger
    Key ceiling( Key key ) {
        if ( root == NULL ) return NULL;
        Node* x = root;
        Node* currentSatisfied = NULL;
        while ( x ) {
            if ( x->key < key ) x = x->right;
            else if ( x->key == key ) return x->key;
            else {
                if ( currentSatisfied == NULL || currentSatisfied->key > x->key ) 
                    currentSatisfied = x;
                if ( !x->left ) return x->key;
                else x = x->left;
            }
        }
        return ( currentSatisfied == NULL ) ? NULL : currentSatisfied->key;
    }

    int rank( Key key ) {
        if ( root == NULL ) return NULL;
        Node* x = root;
        while ( x ) {
            if ( x->key > key ) x = x->left;
            else if ( x->key < key ) x = x->right;
            else return size( x->left );
        }
        return ( !x ) ? 0 : 1 + size( x->left );
    }

    Queue< Key > keys() {
        Node* x = NULL;
        Queue< Key > sortedKeys;
        Stack< Node* > backtrackNodes;

        if ( !root ) throw out_of_range( "keys(): Tree is empty!" );
        if ( root->left ) {
            x = root->left;
            while ( true ) {
                while ( x ) {
                    while ( x->left ) {
                        backtrackNodes.push( x );
                        x = x->left;
                    }
                    sortedKeys.enqueue( x->key );
                    x = x->right;
                }
                while ( !backtrackNodes.isEmpty() ) {
                    if ( !backtrackNodes.peek()->right ) sortedKeys.enqueue( backtrackNodes.pop()->key );
                    else {
                        sortedKeys.enqueue( backtrackNodes.peek()->key );
                        break;
                    }
                }
                if ( backtrackNodes.isEmpty() ) break;
                x = backtrackNodes.pop()->right;
            }
        }
        sortedKeys.enqueue( root->key );
        if ( root->right ) {
            x = root->right;
            while ( true ) {
                while ( x ) {
                    while ( x->left ) {
                        backtrackNodes.push( x );
                        x = x->left;
                    }
                    sortedKeys.enqueue( x->key );
                    x = x->right;
                }
                while ( !backtrackNodes.isEmpty() ) {
                    if ( !backtrackNodes.peek()->right ) sortedKeys.enqueue( backtrackNodes.pop()->key );
                    else {
                        sortedKeys.enqueue( backtrackNodes.peek()->key );
                        break;
                    }
                }
                if ( backtrackNodes.isEmpty() ) break;
                x = backtrackNodes.pop()->right;
            }
        }
        return sortedKeys;
    }

};

int main( int argc, char** argv ) {
    
    BinarySearchTree< int, int > bst;

    cout << left << setw( 21 ) << "The data coming in: ";
    // int data;
    // while ( cin >> data ) {
        // cout << data << " ";
        // bst.put( data, 1 );
    // }
    int n = stoi( argv[ 1 ] );
    for ( int i = 0; i < n; i++ ) {
        int data = randomUniformDistribution( 1, n );
        cout << data << " ";
        bst.put( data, i );
    }
    cout << endl << endl;

    cout << left << setw( 21 ) << "Tree.keys(): ";
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << ". Current tree size: " << bst.size() << endl;
    cout << endl << endl;

    cout << left << setw( 21 ) << "eraseMin(): ";
    bst.eraseMin();
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << ". Current tree size: " << bst.size() << endl;
    cout << endl << endl;

    cout << left << setw( 21 ) << "eraseMax(): ";
    bst.eraseMax();
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << ". Current tree size: " << bst.size() << endl;
    cout << endl << endl;

    cout << left << setw( 21 ) << "erase( 3 ): ";
    bst.erase( 3 );
    for ( auto s : bst.keys() ) {
        cout << s << " ";
    }
    cout << ". Current tree size: " << bst.size() << "          (size unchanged if key not exist to delete)" << endl;
    cout << endl << endl;

    cout << left << setw( 21 ) << "ceiling( 4 ): " << left << setw( 4 ) << bst.ceiling( 4 )  << "       (return NULL (= 0 for type int) if can't find)" << endl << endl;
    cout << left << setw( 21 ) << "floor( 5 ): " << left << setw( 4 ) << bst.floor( 5 ) << "       (return NULL (= 0 for type int) if can't find)"  << endl << endl;

    cout << left << setw( 21 ) << "get( 4 ): " << left << setw( 4 ) << bst.get( 4 ) << "       (return NULL (= 0 for type int) if can't find)"  << endl << endl;
    cout << left << setw( 21 ) << "select( 2 ): " << left << setw( 4 ) << bst.select( 2 ) << "       (return NULL (= 0 for type int) if can't find)"  << endl << endl;

    cout << left << setw( 21 ) << "rank( 5 ): " << left << setw( 4 ) << bst.rank( 5 ) << "       (return NULL (= 0 for type int) if can't find)" << endl << endl;

    return 0;

}
