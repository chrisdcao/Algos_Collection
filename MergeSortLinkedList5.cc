#include <iostream>
#include "SinglyLinkedListQueue.h"
#include "Random.h"

using namespace std;

#define List SinglyLinkedList

template <typename T>
class Merge {
    using NodePtr = SinglyLinkedList<int>::Node*;
private:

    static void FrontBackSplit(NodePtr source, NodePtr* frontRef, NodePtr* backRef) {
        NodePtr slow = source;
        NodePtr fast = source->next;

        while(fast != NULL) {
            fast = fast->next;
            if (fast != NULL) {
                slow = slow->next;
                fast = fast->next;
            }
        }

        *frontRef = source;
        *backRef = slow->next;
        slow->next = NULL;
    }

    static NodePtr merge(NodePtr a, NodePtr b) {
        NodePtr result = NULL;
        if (b == NULL) return a;
        else if (a == NULL) return b;

        if (a->item <= b->item) {
            result = a;
            result->next = merge(a->next, b);
        } else {
            result = b;
            result->next = merge(a, b->next);
        }
        return result;
    }

    static void sort(NodePtr* headRef) {
        /* head will be the running one (change after each recursion)
         * while headRef will be the constant one (to be assigned at the end) */
        NodePtr head = *headRef;
        NodePtr a,b;
        if (head == NULL || head->next == NULL) return;
        FrontBackSplit(head, &a, &b);
        sort(&a);
        sort(&b);
        *headRef = merge(a, b);
    }

public:

    static void sort(List<T>& org) {
        NodePtr* headRef = &org.head;
        sort(headRef);
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    List<int> intList;
    for (int i = 0; i < N; i++)
        intList.insert(randomUniformDistribution(0, N-1));

    cout << "The array before sorting is: ";
    cout << intList << endl;
    
    Merge<int>::sort(intList);

    cout << "The array after sorting is: ";
    cout << intList << endl;

    return 0;

}
