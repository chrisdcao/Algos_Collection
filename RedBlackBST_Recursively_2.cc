#include <iostream>
#include <vector>
#include <numeric>
#include <memory>
#include "Random.h"
#include "Queue.h"

using namespace std;

template <typename Key, typename Value>
class RedBlackBST 
{
    static const bool RED = true;
    static const bool BLACK =  false;

private:

    class Node 
    {
    public:
        Key key;
        Value val;
        Node* left = NULL;
        Node* right = NULL;
        bool color = RED;
        int index = -1;
        int N = 0;
    };

    void freeNode(Node* x)  
    {
        if (x->index < allNodes.size()-1) 
        {
            allNodes[x->index].swap(allNodes.back());
            allNodes[x->index]->index = x->index;
        }
        allNodes.pop_back();
    }

    Node* newNode(Key key, Value val, int N, bool color) 
    {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size()-1;

        allNodes.back()->key = key;
        allNodes.back()->val = val;
        allNodes.back()->N = N;
        allNodes.back()->color = color;

        return allNodes.back().get();
    }

    vector<unique_ptr<Node>> allNodes;
    Node* root = NULL;

    int height(Node* h, int count) 
    {
        if (!h) return count;

        int leftHeight = height(h->left, count+1);
        int rightHeight = height(h->right, count+1);

        return leftHeight > rightHeight ? leftHeight : rightHeight;
    }

    bool isRed(Node* h) 
    {
        return h ? h->color == RED : BLACK;
    }

    int size(Node* h) 
    {
        return h ? h->N : 0;
    }

    Node* put(Node* h, Key key, Value val) 
    {
        if (!h) return newNode(key, val, 1, RED);

        if (h->key > key) 
            h->left = put(h->left, key, val);
        else if (h->key < key) 
            h->right = put(h->right, key, val);
        else 
            h->val = val;
       
        if (isRed(h->right) && !isRed(h->left)) 
            h = rotateLeft(h);
        if (isRed(h->left) && isRed(h->left->left)) 
            h = rotateRight(h);
        if (isRed(h->left) && isRed(h->right)) 
            flipColors(h);

        h->N = size(h->left) + size(h->right) + 1;

        return h;
    }

    Node* rotateRight(Node* h) 
    {
        Node* x = h->left;
        h->left = x->right;
        x->right = h;
        x->color = h->color;
        h->color = RED;
        x->N = h->N;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    Node* rotateLeft(Node* h) 
    {
        Node* x = h->right;
        h->right = x->left;
        x->left = h;
        x->color = h->color;
        h->color = RED;
        x->N = h->N;
        h->N = size(h->left) + size(h->right) + 1;
        return x;
    }

    void flipColors(Node* h) 
    {
        h->color = !h->color;
        h->left->color = !h->left->color;
        h->right->color = !h->right->color;
    }

    Node* deleteMax(Node* x) 
    {
        if (!x) return NULL;

        if (!x->right) 
        {
            freeNode(x);
            return NULL;
        }

        if (!isRed(x->right) && !isRed(x->right->left))
            x = moveRedRight(x);

        x->right = deleteMax(x->right);

        x->N = size(x->left) + size(x->right) + 1;

        return x;
    }

    Node* moveRedRight(Node* h) 
    {// assume that h is red and both children are black
        flipColors(h);
        // check if we have anything to offer for move red Right on the left side
        if (isRed(h->left->left)) 
        {
            h = rotateRight(h);
            flipColors(h);
        }

        return h;
    }

    Node* deleteMin(Node* x) 
    {
        if (!x) return NULL;

        if (!x->left) 
        {
            freeNode(x);
            return NULL;
        }

        if (!isRed(x->left) && !isRed(x->left->left)) 
            x = moveRedLeft(x);
        
        x->left = deleteMin(x->left);

        x->N = size(x->left) + size(x->right) + 1;
        return x;
    }

    Node* moveRedLeft(Node* h) 
    {// assume that h is red, and both children are black
        flipColors(h);
        // check if we have anything to offer for move red Left on the right side
        if (isRed(h->right->left)) 
        {
            h->right = rotateRight(h->right);
            h = rotateLeft(h);
            flipColors(h);
        }
        return h;
    }

    Node* deleteKey(Node* h, Key key) 
    {
        if (!h) return NULL;

        if (h->key > key) 
        {
            if (!h->left) return NULL;

            if (!isRed(h->left) && !isRed(h->left->left)) 
                h = moveRedLeft(h);

            h->left = deleteKey(h->left, key);
        }
        else 
        {
            if (isRed(h->left))
                h = rotateRight(h);

            if (h->key == key && !h->right)  
            {
                freeNode(h);
                return NULL;
            }

            if (!isRed(h->right) && !isRed(h->right->left)) 
                h = moveRedRight(h);

            if (h->key == key) 
            {
                Node* temp = h;
                h = min(temp->right);
                h->right = deleteMin(temp->right);
                h->left = temp->left;
                freeNode(temp);
            }
            else h->right = deleteKey(h->right, key);
        }

        h->N = size(h->right) + size(h->left) + 1;
        return h;
    }

    Node* max(Node* h) 
    {
        return h->right ? max(h->right) : h;
    }

    Node* min(Node* h) 
    {
        return h->left ? min(h->left) : h;
    }

    Node* get(Node* h, Key key) 
    {
        if (!h) return NULL;

        if (h->key > key)
            return get(h->left, key);
        else if (h->key < key)
            return get(h->right, key);
        else 
            return h;
    }

    Node* select(Node* h, int k) 
    {
        if (!h) return NULL;
        int t = size(h->left);

        if (t > k) 
            return select(h->left, k);
        else if (t < k)
            return select(h->right, k-t-1);
        else 
            return h;
    }

    int rank(Node* h, Key key) 
    {
        if (!h) return 0;

        if (h->key > key) 
            return rank(h->left, key);
        else if (h->key < key)
            return 1 + size(h->left) + rank(h->right, key);
        else 
            return size(h->left);
    }

    Node* ceiling(Node* h, Key key) 
    {
        if (!h) return NULL;

        if (h->key < key)
            return ceiling(h->right, key);
        else if (h->key == key)
            return h;
        Node* temp = ceiling(h->left, key);

        return temp ? temp : h;
    }

    Node* floor(Node* h, Key key) 
    {
        if (!h) return NULL;

        if (h->key > key) 
            return floor(h->left, key);
        else if (h->key == key)
            return h;
        Node* temp = floor(h->right, key);

        return temp ? temp : h;
    }

    void keys(Node* h, Queue<Key>& queue, Key lo, Key hi) 
    {
        if (!h) return;
        if (h->key < lo) return;
        if (h->key > hi) return;
        keys(h->left, queue, lo, hi);
        queue.enqueue(h->key);
        keys(h->right, queue, lo, hi);
    }

public:

    RedBlackBST() {}

    virtual ~RedBlackBST() {}

    int height() 
    {
        return height(root, -1);
    }

    int size() 
    {
        return size(root);
    }

    bool isEmpty() 
    {
        return size() == 0;
    }

    void put(Key key, Value val) 
    {
        root = put(root, key, val);
        root->color = BLACK;
    }

    void deleteMin() 
    {
        if (!root) return;

        if (!isRed(root->left) && !isRed(root->right))
            root->color = RED;

        root = deleteMin(root);

        if (!isEmpty()) root->color = BLACK;
    }

    void deleteMax() 
    {
        if (!root) return;
        
        if (!isRed(root->left) && !isRed(root->right))
            root->color = RED;

        root = deleteMax(root);

        if (!isEmpty()) root->color = BLACK;
    }

    void deleteKey(Key key) 
    {
        if (isEmpty()) return;

        if (!isRed(root->right) && !isRed(root->left)) 
            root->color = RED;

        root = deleteKey(root,key);

        if (!isEmpty()) root->color = BLACK;
    }

    Key max() 
    {
        Node* rv = max(root);
        return rv ? rv->key : throw runtime_error("max(): No Key Found");
    }

    Key min() 
    {
        Node* rv = min(root);
        return rv ? rv->key : throw runtime_error("min(): No Key Found");
    }

    Value get(Key key) 
    {
        Node* rv = get(root, key);
        return rv ? rv->val : throw runtime_error("get(): No Key Found!");
    }

    Key select(int k) 
    {
        Node* rv = select(root, k);
        return rv ? rv->key : throw runtime_error("select(): No Key Found");
    }

    bool contains(Key key) 
    {
        return get(root, key);
    }

    Key floor(Key key) 
    {
        return floor(root, key)->key;
    }

    Key ceiling(Key key) 
    {
        return ceiling(root, key)->key;
    }

    Queue<Key> keys(Key lo, Key hi) 
    {
        Queue<Key> queue;
        keys(root, queue, lo, hi);
        return queue;
    }

    Queue<Key> keys() 
    {
        return keys(min(),max());
    }

    int rank(Key key) 
    {
        return rank(root, key);
    }

};

template< typename Key, typename Value >
const bool RedBlackBST< Key, Value >::RED;

template< typename Key, typename Value >
const bool RedBlackBST< Key, Value >::BLACK;

int main( int argc, char ** argv ) 
{

    string keyArr[] = { "S", "E", "A", "R", "C", "H", "X", "M", "P", "L", "L", "L", "L" };
    string valueArr[] = { "S", "E", "A", "R", "C", "H", "X", "M", "P", "L", "L", "L", "L" };

    RedBlackBST< string, string > bst;
    for ( int i = 0; i < sizeof( keyArr ) / sizeof( string ); ++i ) 
        bst.put( keyArr[ i ], valueArr[ i ] );

    cout << "DEBUG: bst: " << endl;
    for (auto s : bst.keys())
        cout << s << " ";
    cout << endl;

    cout << "DEUBG: bst.size(): ";
    cout << bst.size() << endl;

    cout << "DEBUG: bst.height(): ";
    cout << bst.height() << endl;

    cout << "DEBUG: bst.min(): ";
    cout << bst.min() << endl;

    cout << "DEBUG: bst.max(): ";
    cout << bst.max() << endl;

    cout << "DEBUG: bst.get( L ): ";
    cout << bst.get( "L" ) << endl;

    cout << "DEBUG: bst.floor( C ): ";
    cout << bst.floor( "C" ) << endl;

    cout << "DEBUG: bst.ceiling( C ): ";
    cout << bst.ceiling( "C" ) << endl;

    cout << "DEBUG: bst.keys(): ";
    for (auto s : bst.keys())
        cout << s << " ";
    cout << endl;

    bst.deleteMin();
    cout << "DEBUG: bst.keys() (AFTER deleteMin()): ";
    for (auto s : bst.keys())
        cout << s << " ";
    cout << endl;

    bst.deleteMax();
    cout << "DEBUG: bst.keys() (AFTER deleteMax()): ";
    for (auto s : bst.keys())
        cout << s << " ";
    cout << endl;

    cout << "DEBUG: bst.floor( C ): " << bst.floor( "C" ) << endl;
    cout << "DEBUG: bst.floor( D ): " << bst.floor( "D" ) << endl;

    cout << "DEBUG: bst.contains( C ): " << bst.contains( "C" ) << endl;
    cout << "DEBUG: bst.contains( D ): " << bst.contains( "D" ) << endl;

    bst.deleteKey( "L" );
    cout << "DEBUG: bst.delete( L ): ";
    for (auto s : bst.keys())
        cout << s << " ";
    cout << endl;

    cout << "DEBUG: bst.rank( S ): " << bst.rank( "S" ) << endl;
    cout << "DEBUG: bst.select( 5 ): " << bst.select( 5 ) << endl;

    return 0;
}
