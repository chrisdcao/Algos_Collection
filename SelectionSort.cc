#include <iostream>
#include <algorithm>
#include <vector>
#include "Random.h"

using namespace std;

template <typename T>
class Selection {
public:
    static void sort(vector<T>& vec) {
        int N = vec.size();
        for (int i = 0; i < N; i++) {
            int iMin = i;
            for (int j = i+1; j < N; j++) {
                if (vec[j] < vec[iMin]) 
                    iMin = j;
            }
            swap(vec[i], vec[iMin]);
        }
    }
};

int main(int argc, char** argv) {
    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0,N-1));
    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;
    Selection<int>::sort(vec);
    cout << "\nThe array after sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;
}
