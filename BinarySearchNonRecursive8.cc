#include <iostream>
using namespace std;


int BinarySearch(int key, int a[], int lo, int hi) {
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key > a[mid]) lo = mid + 1;
        else if (key < a[mid]) hi = mid - 1;
        else return mid;
    }
    return -1;
}

int main () {
    int a[] = {1, 2, 2, 5, 6, 6, 8, 9, 12};
    int lo = 0;
    int hi = sizeof(a) / sizeof(a[0]);
    cout << BinarySearch(5, a, lo, hi) << endl;
    cout << BinarySearch(1, a, lo, hi) << endl;
    cout << BinarySearch(12, a, lo, hi) << endl;
    cout << BinarySearch(17, a, lo, hi) << endl;
}
