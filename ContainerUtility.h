#ifndef CONTAINERUTILITY_H
#define CONTAINERUTILITY_H

#include <iostream>
#include <vector>
#include <iterator>
#include <utility>
#include <iomanip>

using namespace std;

ostream& operator<<(ostream& os, vector<int>& vec) {
    for (auto s : vec)
        os << s << " ";
    return os;
}

template <typename T=int>
void vecRangePrint(vector<T>& a, int lo, int hi) {
    for (int i = lo; i < hi; i++)
        cout << a[i] << " ";
    cout << endl;
}

template <typename N>
bool smaller(N* a, int i, int j) {
    return a[i] < a[j]; 
}

template <typename M>
void exch(M* a, int i , int j) {
    M t = a[i];
    a[i] = a[j];
    a[j] = t;
}

#endif
