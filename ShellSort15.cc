#include <iostream>
#include <vector>

using namespace std;

template <typename T>
class Shell {
    static void sort(vector<T>& org) {
        int N = org.size();
        int h = 1;
        while (h < N/3) h = h * 3 + 1;
        while (h >= 1) {
            for (int i = h; i < N; i++)  {
                for (int j = i; j > 0 && a[j] < a[j-h]; j--)
                    swap(a[j], a[j-h]);
            }
            h /= 3;
        }
    }
};

int main(int argc, char** argv) {
    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0,N-1));
    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;
    Shell<int>::sort(vec);
    cout << "\nThe array after sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;
}
