// Merge sort top down run in a tree-like manner
// which means that each recursion they execute both sides of the range
// making it 'branch' into 2x recursion and keep going like that
// so all the element will be sorted eventually
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <random>
#include <vector>
#include <numeric>

using namespace std;

template <typename T>
class Merge {
private:
    
    static void merge(vector<T>& orgVec, vector<T>& auxVec, int low, int mid, int high) {
        for (int k = low; k <= high; ++k)
            auxVec[k] = orgVec[k];

        int i = low;
        int j =mid+1;;
        // merge back to orgVec
        for (int k = low; k <= high; ++k) {
            // if the index i > mid that means there's no element left from i-sub-array (which has an end-index of 'mid'), meaning we have to take from the j-sub-array no matter what
            if      (i > mid)               { orgVec[k] = auxVec[j++]; }
            // if index j > high means there's no element left from the j-sub-array (which has an end-index of high), meaning we have to take from the i-sub-array no matter what
            else if (j > high)              { orgVec[k] = auxVec[i++]; }
            // these are to check whatever elements from which sub-array are smaller, we put it to orgVec first
            else if (auxVec[i] > auxVec[j]) { orgVec[k] = auxVec[j++]; }
            else                            { orgVec[k] = auxVec[i++]; }
        }
    }

    static void sort(vector<T>& orgVec, vector<T>& auxVec, int low, int high) {
        if(high <= low) return;
        int mid = low + (high - low) / 2;
        sort(orgVec, auxVec, low, mid);
        sort(orgVec, auxVec, mid+1, high);
        merge(orgVec, auxVec, low, mid, high);
    }

public:

    static void sort(vector<T>& orgVec) {
        vector<T> auxVec(orgVec.size());
        sort(orgVec, auxVec, 0, orgVec.size() - 1);
    }
};

// Merge sort
int main(int argc, char** argv) {

    vector<int> arr(stoi(argv[1]));
    iota(arr.begin(), arr.end(), 0);

    cout << "Before sorting: ";
    random_shuffle(arr.begin(),arr.end());
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    Merge<int>::sort(arr);
    // mergesort function
    cout << "After sorting: ";
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    return 0;

}
