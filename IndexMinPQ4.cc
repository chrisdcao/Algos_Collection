#include <iostream>
#include <string>
#include <numeric>
#include <vector>
#include <iomanip>

using namespace std;

template <typename Key> class IndexMinPQ {

private:

    vector< int > pq;
    vector< int > qp;
    vector< Key > keys;
    int N = 0;

    bool greater( int i, int j ) {
        return keys[ i ] > keys[ j ];
    }

    void exchange( int i, int j ) {
        int temp = pq[ i ];
        pq[ i ] = pq[ j ];
        pq[ j ] = temp;
        qp[ pq[ i ] ] = i;
        qp[ pq[ j ] ] = j;
    }

    void sink( int k ) {
        while ( 2 * k <= N ) {
            int j = 2 * k;
            if ( j < N && greater( pq[ j ], pq[ j+1 ] ) ) j++;
            if ( ! greater( pq[ k ], pq[ j ] ) ) break;
            exchange( k, j );
            k = j;
        }
    }

    void swim( int k ) {
        while ( k > 1 && greater( pq[ k/2 ], pq[ k ] ) ) {
            exchange( k/2, k );
            k /= 2;
        }
    }

public:

    IndexMinPQ( int maxN ): pq( maxN+1 ), qp( maxN+1 ), keys( maxN+1 ) {}

    virtual ~IndexMinPQ() {}

    int size() { return N; }

    bool isEmpty() { return size() == 0; }
    
    bool contains( int k ) { return qp[ k ] != -1; }

    void insert ( int k, Key key ) {
        N++;
        keys[ k ] = key;
        pq[ N ] = k;
        qp[ k ] = N;
        swim( N );
    }

    int getMinIndex() { return pq[ 1 ]; }

    Key getMin() { return keys[ pq[ 1 ] ]; }

    int delMin() {
        int min = pq[ 1 ];
        exchange( 1, N-- );
        sink( 1 );
        pq[ N+1 ] = -1;
        qp[ min ] = -1;
        return min;
    }

    void deleteKey( int k ) {
        if ( ! contains( k ) ) throw invalid_argument( "deleteKey(): Invalid Key" );
        int index = qp[ k ];
        exchange( index, N-- );
        swim( index );
        sink( index );
        pq[ N+1 ] = -1;
        // sau exchange thì qp vẫn là nghịch đảo của pq ( bởi trong exchange chúng ta cũng hoán luôn của qp )
        // nên nó trở thành nghịch đảo của nghịch đảo ( pq bị nghịch đảo và qp là nghịch đảo của pq ) nên các vị trí xóa của qp luôn là vị trí được nhập vào orginal thay vì là vị trí sau exchange ( 2 ngược = 1 thuận )
        qp[ k ] = -1;
    }


    void change( int k, Key key ) {
        if ( ! contains( k ) ) throw invalid_argument( "change(): Invalid index" );
        keys[ k ] = key;
        swim( qp[ k ] );
        sink( qp[ k ] );
    }

    friend ostream& operator <<( ostream& os, const IndexMinPQ< Key >& minPQ ) {
        for ( int i = 0; i < minPQ.pq.size(); i++ ) 
            os << i << " : " << minPQ.pq[ i ] << endl;

        os << "\nqp:\n";
        for( int i = 0; i < minPQ.qp.size(); i++ )
            os << i << " : " << minPQ.qp[ i ] << endl;

        os << "\nkeys:\n";
        for( int i = 0; i < minPQ.keys.size(); i++ )
            os << i << " : " << minPQ.keys[ i ] << endl;
        
        return os;
    }

};

int main ( int argc, char** argv ) {

    int intArr[] = { 14, 12, 10, 8, 6, 4, 2, 0, 1, 3, 5, 7, 9, 11, 13 };
    IndexMinPQ< int > minPQ( sizeof( intArr ) / sizeof( int ) );
    for ( int i = 0; i < sizeof( intArr ) / sizeof( int ); ++i ) {
        minPQ.insert( i, intArr[ i ] );
    }

    cout << "DEBUG: minPQ: \n" << minPQ << endl;
    minPQ.deleteKey( 4 );
    cout << "DEBUG: minPQ.deleteKey( 4 ): \n" << minPQ << endl;
    minPQ.change( 3, 20 );
    cout << "DEBUG: minPQ.change( 3, 20 ): \n" << minPQ << endl;
    cout << "DEBUG: minPQ.getMin(): \n" << minPQ.getMin() << endl;
    cout << "DEBUG: minPQ.getMinIndex(): \n" << minPQ.getMinIndex() << endl;
    cout << "DEBUG: minPQ.delMin(): \n" << minPQ.delMin() << endl;
    cout << "DEBUG: minPQ.contains( 4 ): \n" << minPQ.contains( 4 ) << endl;
    cout << "DEBUG: minPQ.contains( 10 ): \n" << minPQ.contains( 10 ) << endl;

    return 0;

}
