#include <iostream>
#include <algorithm>
#include <random>
#include "Random.h"

using namespace std;

template <typename T>
class Quick {
private:

    static int partition(vector<T>& a, int lo, int hi) {
        int i = lo, j = hi + 1;
        T v = a[lo];
        while (true) {
            /* the exercise suggests that at the public sort() function, before calling the private sort(), we find max, then move the max into a[length-1], so that the pivot can never be the largest element (and thus will never go out of bound)
             * there's only one of this element, or do we generate this sentinel at each end of the sub-array */
            while (a[++i] < v) if (i == hi) break;
            /* the exercise says this test is not essential because the pivot has already acted as the sentinel
             * v is never less than a[lo] because in the smallest case (where the array is sorted, that's when pivot is the smallest, it is at least == v, cannot be less). So we don't need to set a bound for this pointer */
            while (v < a[--j]) if (j == lo) break;
            if (i >= j) break;
            swap(a[i], a[j]);
        }
        swap(a[lo], a[j]);
        return j;
    }

    static void sort(vector<T>& a, int lo, int hi) {
        if (lo >= hi) return;
        int j = partition(a, lo, hi);
        sort(a, lo, j-1);
        sort(a, j+1, hi);
    }

public:

    static void sort(vector<T>& a) {
        random_shuffle(a.begin(), a.end());
        T currentMax = a[0];
        int currentMaxIndex = 0;
        for (int i = 0; i < a.size(); i++) {
            if (a[i] > currentMax) {
                currentMax = a[i];
                currentMaxIndex = i;
            }
        }
        swap(a[currentMaxIndex], a[a.size()-1]);
        sort(a, 0, a.size() - 1);
    }

};

int main(int argc, char** argv) {

    vector<int> vec;
    int N = stoi(argv[1]);
    for (int i = 0;i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));

    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " "; 
    cout << endl;

    Quick<int>::sort(vec);

    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " "; 
    cout << endl;

    return 0;

}

