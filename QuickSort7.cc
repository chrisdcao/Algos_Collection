#include <iostream>
#include <random>
#include <algorithm>
#include <vector>

using namespace std;

template <typename T>
class Quick {
private:

    static void sort(vector<T>& a, int lo, int hi) {
        if (hi <= lo) return;
        int j = partition(a, lo, hi);
        sort(a, lo, j);
        sort(a, j+1, hi);
    }

    /* this partition is to keep looking and sorting on the 2 sides of the median of the current array */
    static void partition(vector<T>& a, int lo, int hi) {
        int i = lo, j = hi + 1; 
        T v = a[lo];
        while (true) {
            while (a[++i] < v) if (i == hi) break;
            while (v < a[--j]) if (j == lo) break;
            if (i >= j) break;
            swap(a[i], a[j]);
        }
        swap(a[lo], a[j]);;
    }

public:

    static sort(vector<T>& a) {
        shuffle(a.begin(), a.end());
        sort(a, 0, a.size()-1);
    }

};
