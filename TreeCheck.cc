#include <iostream>
#include <vector>

using namespace std;

bool isBalancedTree(Node* x) {
    
}

// the subtree consistency tests if the left subtree + right subtree == parent size
bool isSubtreeConsistent(Node* x) {
    if (!x) return true;

    int totalCount = 0;
    if (x->left) {
        totalCount += size(x->left);
    }
    if (x->right) {
        totalCount += size(x->right);
    }

    // usually we have the formula of: current_size = size(left) + size(right) + 1;
    // thus to compare the current_size with Sum(left_size + right_size), we have to plus 1 into Sum
    if (size(x) != totalCount + 1) return false;

    return isSubtreeConsistent(x->left) && isSubtreeConsistent(x->right);
}

// these are the properties of RB234Tree
// learn more about properties in the videos
bool isValid234Tree(Node* x) {
    if (!x) return true;
    // red black tree allow the 2 red links on 2 sides of the node so we don't have to check for that condition
    if (isRed(x->right) && !isRed(x->left)) return false;
    if (isRed(x->left) && isRed(x->left->left)) return false;
    if (isRed(x->left) && isRed(x->left->right)) return false;
    if (isRed(x->right) && isRed(x->right->right)) return false;
    if (isRed(x->right) && isRed(x->right->left)) return false;
    return isValid234Tree(x->left) && isValid234Tree(x->right);
}

bool isSubtreeConsistent(Node* x) {
    if (!x) return true;

    int totalCount = 0;
    if (x->left) {
        totalCount += size(x->left);
    }
    if (x->right) {
        totalCount += size(x->right);
    }

    if (size(x) != totalCount + 1) return false;

    return isSubtreeConsistent(x->left) && isSubtreeConsistent(x->right);
}

