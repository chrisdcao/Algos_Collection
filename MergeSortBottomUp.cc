#include <iostream>
#include <vector>
#include <numeric>
#include <iomanip>
#include <random>
#include <algorithm>

using namespace std;

// Bottom up means we sort from the pair up? (up to 4 then 8 then .. N/2 then N)
template <typename T>
class MergeBottomUp {
private:

    static void merge(vector<T>& orgVec, vector<T>& auxVec, int low, int mid, int high) {
        for (int k = low; k <= high; ++k) { auxVec[k] = orgVec[k]; }
        int i = low;
        int j = mid+1;
        for (int k = low; k <= high; ++k) {
            if  (i > mid) orgVec[k] = auxVec[j++];
            else if (j > high) orgVec[k]  = auxVec[i++];
            else if (auxVec[j] > auxVec[i]) orgVec[k] = auxVec[i++];
            else orgVec[k] = auxVec[j++];
        }
    }

public:

    // this is not a recursive function anymore
    static void sort(vector<T>& orgVec) {
        //we take the size of the vector
        int N = orgVec.size();
        vector<T> auxVec(N); 
        for  (int len = 1; len < N; len *= 2) {
            // we sort smaller to bigger cluster of the whole array first (instead sorting left then right side like what the recursion have done)
            // low go 2 * Distance so that it doesn't cross the previously 'sorted values'
            for (int low = 0; low < N - len; low += len + len) {
                int mid = low + len - 1;
                // this is to prevent it from going out the array
                int high = min(low + len + len - 1, N - 1);
                merge(orgVec, auxVec, low, mid, high);
            }
        }
    }

};

// Merge sort
int main(int argc, char** argv) {

    vector<int> arr(stoi(argv[1]));
    iota(arr.begin(), arr.end(), 0);

    cout << "Before sorting: ";
    random_shuffle(arr.begin(),arr.end());
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    MergeBottomUp<int>::sort(arr);
    // mergesort function
    cout << "After sorting: ";
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    return 0;

}
