#include "Common/Graph.h"
#include <queue>

using namespace std;

#define EWD EdgeWeightedDirectedGraph
#define EWDC EdgeWeightedDirectedCycle

class BellmanFordSP {
public:
	vector<bool> marked;
	vector<Edge*> edgeTo;
	vector<double> cost;
	vector<bool> onQ;
	int passCnt=0;
	queue<int> q;
	vector<Edge> cycle;

	BellmanFordSP(EWD& G,int s): marked(G.V), 
								 edgeTo(G.V), 
								 cost(G.V), 
								 onQ(G.V) {
		fill(cost.begin(), cost.end(), DBL_MAX);
		fill(edgeTo.begin(), edgeTo.end(), nullptr);
		cost[s]=0.0;
		q.push(s);
		onQ[s]=true;
		while (!q.empty()) {
			int v=q.front();
			q.pop();
			onQ[v]=false;
			relax(G,v);
		}
	}
	~BellmanFordSP() {}

	void relax(EWD& G, int v) {
		for (auto& e : G.adj[v]) {
			if (cost[e.to] > cost[e.from]+e.cost) {
				cost[e.to]=cost[e.from]+e.cost;
				edgeTo[e.to]=&e;
				if (!onQ[e.to]) {
					q.push(e.to);
					onQ[e.to]=true;
				}
			}
			if (++passCnt%G.V==0) {
				if (hasNegativeCycle()) return;
				findNegativeCycle();
			}
		}
	}

	void findNegativeCycle() {
		// INIT new graph <- {all visited edges till now}
		int V = edgeTo.size();
		EWD spt(V);
		for (int v = 0; v<V;v++)
			if (edgeTo[v]!=nullptr)
				spt.addEdge(*edgeTo[v]);
		// find cycle on new graph
		EWDC cycleFinder(spt);
		cycle=cycleFinder.getCycle();
	}

	inline bool hasNegativeCycle() { return !cycle.empty(); }

	inline bool hasPathTo(int v) { return cost[v] < DBL_MAX; }

	vector<Edge> getPathTo(int v) {
		if (hasNegativeCycle()) throw runtime_error("Negative Cycle Detected! Cannot compute path!");
		vector<Edge> vec;
		if (!hasPathTo(v)) return vec;
		for (Edge* x=edgeTo[v]; x!=nullptr; x=edgeTo[(*x).from])
			vec.push_back(*x);
		reverse(vec.begin(), vec.end());
		return vec;
	}

	inline vector<Edge>& getNegativeCycle() { return cycle; }

	inline double getCostTo(int v) {
		if (hasNegativeCycle()) throw runtime_error("Negative Cycle Detected! Cannot compute cost!");
		return cost[v];
	}
};

int V,E,a,b;
double weight;
int main() {
	freopen("tinyEWD.txt", "r", stdin);
	cin >> V >> E;
	EWD G(V);
	for (int i =0; i<E;i++) {
		cin >> a >> b >> weight;
		G.addEdge(a,b,weight);
	}
	//cout << G.toString() << endl;

	BellmanFordSP bf(G,0);
	cout << "Path from 0 to:\n";
	for (auto v = 0; v < G.V;v++) {
		cout << v << ": ";
		for (auto& e : bf.getPathTo(v))
			cout << "(" << e.from << "-" << e.to << ":" << e.cost << "), ";
		cout << endl;
	}

	return (0);
}
