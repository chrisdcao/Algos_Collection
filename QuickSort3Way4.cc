#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include "Random.h"

using namespace std;

template <typename T>
class Quick3Way {
private:

    static void sort(vector<T>& a, int lo, int hi) {
        if (lo >= hi) return;
        /* pivot is fixed in the lo position */
        int left = lo, i = lo+1, right = hi;
        T pivot = a[lo];
        while (i <= right) {
            /* swap and move index forward   */
            if (a[i] < pivot) swap(a[i++], a[left++]);
            /* swap and keep comparing that with pivot */
            else if (a[i] > pivot) swap(a[i], a[right--]);
            else i++;
        }
        /* we don't swap pivot with the middle in this 3way */
        sort(a, lo, left-1);
        sort(a, right+1, hi);
    }

public:

    static void sort(vector<T>& a) {
        random_shuffle(a.begin(), a.end());
        sort(a, 0, a.size()-1);
    }

};

int main(int argc, char** argv) {

    vector<int> vec;
    int N = stoi(argv[1]);
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));

    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    Quick3Way<int>::sort(vec);

    cout << "The array after sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    return 0;

}

