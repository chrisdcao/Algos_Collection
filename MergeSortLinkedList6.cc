#include <iostream>
#include <vector>
#include "SinglyLinkedList.h"

using namespace std;

#define List SinglyLinkedList

template <typename T>
class Merge {
using NodePtr = SinglyLinkedList<int>::Node*;
private:

    static NodePtr merge(NodePtr* headRef, NodePtr a, NodePtr b) {
        NodePtr result = NULL;
        if (a == NULL) return b;
        else if (b == NULL) return a;
        if (a->item >= b->item) {
            result = a;
            result->next = merge(a->next, b);
        } else {
            result = b;
            result->next = merge(a, b->next);
        }
        return result;
    }

    static void FrontBackSplit(NodePtr* headRef, NodePtr* frontRef, NodePtr* backRef) {
        NodePtr fast = source->next,
                slow = source;
        while (fast != NULL) {
            fast = fast->next;
            if (fast != NULL) {
                fast = fast->next;
                slow = slow->next;
            }
        }

        *frontRef = source;
        *backRef = slow->next;
    }

    static NodePtr sort(NodePtr* headRef) {
        NodePtr head = *headRef;
        NodePtr a,b;
        if (head == NULL || head->next == NULL) return;
        FrontBackSplit(head,&a,&b);
        sort(&a);
        sort(&b);
        *headRef = merge(a,b);
    }

public:

    static void sort(List<T>& org) {
        NodePtr* headRef = &org.head;
        sort(headRef);
    }

};

