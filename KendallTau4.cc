#include <iostream>
#include <string>
#include <numeric>
#include <vector>
#include <iomanip>
#include "Inversions.h"
#include "Random.h"

using namespace std;

class KendallTau {
public:

    static long distance(vector<int> a, vector<int> b) {
        if (a.size() != b.size())
            throw logic_error("distance(): array dimensions disagree");
        int n = a.size();

        // a_inverse: the index and value of a are swapped
        vector<int> ainv(n);
        for (int i = 0; i < n; i++) 
            ainv[a[i]] = i;

        vector<int> bnew(n);
        for (int i = 0; i < n; i++)
            bnew[i] = ainv[b[i]];

        // count the number of inversion pairs
        return Inversions::count(bnew);
    }

    // creating a random vector
    static vector<int> permutation(int n) {
        vector<int> a(n);
        for (int i = 0; i < n; i++)
            a[i] = i;
        uniform_shuffle(a);
        return a;
    }

};

int main(int argc, char** argv) {

    int n = stoi(argv[1]);
    vector<int> a = KendallTau::permutation(n);
    vector<int> b = KendallTau::permutation(n);

    for (int i = 0; i < n; i++)
        cout << a[i] << " " << b[i] << endl;
    cout << endl;

    // inversions are the pairs that have 'invert' relationship
    cout << "inversions = " << KendallTau::distance(a, b) << endl;

    return 0;

}
