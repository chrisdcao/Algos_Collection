#include <iostream>
#include <vector>
#include "Random.h"
#include "ContainerUtility.h"
#include "Stack.h"

using namespace std;

template <typename Key>
class MaxPQ {

private:

    Key* pq;
    int N = 0;
    int capacity = 0;

    void swim(int k) {
        while (k > 1 && pq[k/2] < pq[k]) {
            swap(pq[k/2], pq[k]);
            k = k/2;
        }
    }

    void sink(int k) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j] < pq[j+1]) j++;
            if (!(pq[k] < pq[j])) break;
            swap(pq[k], pq[j]);
            k = j;
        }
    }

    /* dynamic resizing of priority Queue */
    void resize(int max) {
        Key* temp = new Key[max];
        for (int i = 0; i < max; i++)
            temp[i] = pq[i];
        delete[] pq;
        pq = temp;
    }

public:

    /* constructors */
    MaxPQ(): N(0), capacity(2) { pq = new Key[2]; }

    MaxPQ(int maxN): N(0), capacity(maxN+1) { pq = new Key[maxN+1]; }

    /* basic size-related function */
    bool isEmpty() { return N == 0; }

    int size() { return N; }

    /* insert and remove functions */
    void insert(Key v) {
        if (N == capacity) {
            capacity *= 2;
            resize(capacity);
        }
        pq[++N] = v;
        swim(N);
    }

    Key delMax() {
        if (N == 0) {
            cout << "Underflow. Returning default value for the data type: ";
            return Key();
        }
        if (N == capacity / 4) {
            capacity = capacity / 2;
            resize(capacity);
        }
        Key max = pq[1];
        swap(pq[1], pq[N--]);
        pq[N+1] = Key();
        sink(1);
        return max;
    }

};


/* sample client: only keep the smallest element left */
int main(int argc, char** argv) {

    int M = stoi(argv[1]);
    MaxPQ<int> pq(M + 1);
    int x;
    while (cin >> x) {
        pq.insert(x);
        if (pq.size() > M)
            pq.delMax();    
    }
    Stack<int> stack;
    while (!pq.isEmpty()) stack.push(pq.delMax());
    for (auto t : stack) cout << t << endl;

    return 0;

}
