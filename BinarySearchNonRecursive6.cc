#include <iostream>
#include <vector>

using namespace std;

int BinarySearch(int key, vector<int> vec) {
    int lo = 0;
    int hi = vec.size() - 1;
    while (lo <= hi) {
        int mid = lo + (hi - lo) / 2;
        if (key < vec[mid]) hi = mid - 1;
        else if (key > vec[mid]) lo = mid + 1;
        else return mid;
    }
    return -1;
}

int main() {
    vector<int> vec = {1, 2, 3, 4, 5, 6, 7};
    cout << BinarySearch(1, vec) << endl;
    cout << BinarySearch(7, vec) << endl;
    cout << BinarySearch(9, vec) << endl;
}
