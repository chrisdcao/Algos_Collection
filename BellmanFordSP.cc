#include "Common/Graph.h"
#include <queue>

#define EWD EdgeWeightedDirectedGraph
#define EWDC EdgeWeightedDirectedCycle

class BellmanFordSP {
public:
	vector<double> cost;
	vector<Edge*> edgeTo;
	vector<bool> onQ;
	queue<int> q;
	int passCnt=0;
	vector<Edge> cycle;

	BellmanFordSP(EWD& G, int s): cost(G.V), edgeTo(G.V), onQ(G.V) {
		fill(cost.begin(), cost.end(), DBL_MAX);
		fill(edgeTo.begin(), edgeTo.end(), nullptr);
		cost[s]=0;
		q.push(s);
		onQ[s]=true;
		while (!q.empty()) {
			int v= q.front();
			q.pop();
			onQ[v]=false;
			relax(G,v);
		}
	}
	~BellmanFordSP() {}

	void relax(EWD& G, int v) {
		for (auto& e : G.adj[v]) {
			int w=e.to;
			if (cost[w] > cost[v]+e.cost) {
				cost[w]=cost[v]+e.cost;
				edgeTo[w]=&e;
				if (!onQ[w]) {
					q.push(w);
					onQ[w]=true;
				}
			}
			if (++passCnt%G.V==0) {
				findNegativeCycle();
				if (hasNegativeCycle()) return;
			}
		}
	}

	inline bool hasNegativeCycle() const { return !cycle.empty(); }

	inline vector<Edge>& getNegativeCycle() { return cycle; }

	void findNegativeCycle() {
		int V = edgeTo.size();
		EWD spt(V);
		for (int v = 0; v<V;v++)
			if (edgeTo[v] != nullptr)
				spt.addEdge(*edgeTo[v]);
		EWDC finder(spt);
		cycle = finder.getCycle();
	}

	double getCostTo(int v) {
		if (hasNegativeCycle())
			throw runtime_error("Has negative cycle, cannot compute cost to v");
		return cost[v];
	}

	inline bool hasPathTo(int v) { return cost[v] < DBL_MAX; }

	vector<Edge> pathTo(int v) {
		vector<Edge> vec;
		if (hasNegativeCycle()) 
			throw runtime_error("Negative cycle exist! Cannot compute path!");
		if (!hasPathTo(v))
			return vec;
		for (Edge* e=edgeTo[v]; e!=nullptr; e=edgeTo[e->from]) 
			vec.push_back(*e);
		reverse(vec.begin(),vec.end());
		return vec;
	}
};

int V,E,a,b;
double weight;

int main() {
	freopen("tinyEWD.txt", "r", stdin);
	cin >> V >> E;
	EWD G(V);
	for (int i =0; i<E;i++) {
		cin >> a >> b >> weight;
		G.addEdge(a,b,weight);
	}
	//cout << G.toString() << endl;

	BellmanFordSP bf(G,0);
	cout << "Path from 0 to ";
	for (auto v = 0; v < G.V;v++) {
		cout << v << ": ";
		for (auto& e : bf.pathTo(v))
			cout << "(" << e.from << "-" << e.to << ":" << e.cost << "), ";
		cout << endl;
	}

	return (0);
}
