#include <iostream>
#include <algorithm>
#include <random>
#include <vector>
#include "Random.h"

using namespace std;

template <typename T>
class Quick3Way {
private:
    
    static void sort(vector<T>& a, int lo, int hi) {
        if (hi <= lo) return;
        /* i will be our running pointer
         * while lt is our pivot point and will be constant, only change when 'lo' is changed */
        int lt = lo, i = lo + 1, gt = hi;
        T v = a[lo];
        /* while the running pointer not reaching the end of the array */
        while (i <= gt) {
            /* if the value is smaller than the pivot value then we swap the value at pivot with the value, and move both up one index */
            if (a[i] < v) swap(a[lt++], a[i++]);
            /* else swap the pointer with the one in the end */
            else if (a[i] > v) swap(a[i], a[gt--]);
            /* else if all the elements are in their right place we keep moving the pointer up */
            else i++;
        }
        sort(a, lo, lt-1);
        sort(a, gt+1, hi);
    }

public:

    static void sort(vector<T>& a) {
        random_shuffle(a.begin(), a.end());
        sort(a, 0, a.size()-1);
    }

};

int main(int argc, char** argv) {

    vector<int> vec;
    int N = stoi(argv[1]);
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));

    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    Quick3Way<int>::sort(vec);

    cout << "The array after sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    return 0;

}

