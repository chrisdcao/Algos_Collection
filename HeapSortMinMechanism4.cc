#include <iostream>
#include <vector>
#include "Random.h"
#include "ContainerUtility.h"

/* MinHeap: parent < child
 * by using MinHeap mechanism, we are sorting for a MaxHeap */
template <typename T>
class MinMechanismHeap {
private:
    /* we have to decrease one because vector starts from 0 instead of 1 like Heap */
    static void sink(vector<T>& pq, int k, int N) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j-1] > pq[j]) j++;
            if (pq[k-1] <= pq[j-1]) break;
            swap(pq[k-1], pq[j-1]);
            k = j;
        }
    }

public:

    static void sort(vector<T>& a) {
        int N = a.size();
        for (int k = N/2; k >= 1; k--)
            sink(a, k, N);
        while (N >= 1) {
            swap(a[0], a[N]);
            sink(a, 1, N);
            N--;
        }
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));
    cout << "The array before sorting: ";
    cout << vec << endl;

    MinMechanismHeap<int>::sort(vec);

    cout << "The array after sorting: ";
    cout << vec << endl;

}
