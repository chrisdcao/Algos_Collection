#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include "Random.h"

using namespace std;

template <typename T>
class Quick3Way {
private:

    static void sort(vector<T>& a, int lo, int hi) {
        if (lo >= hi) return;
        int l = lo, i = lo+1, r = hi;
        T v = a[lo];
        while (i <= r) {
            /* put the current value into the left side */
            if (a[i] < v) swap(a[i++], a[l++]);
            /* put the current value into right side, but keep using the current value to compare with the pivot */
            else if (a[i] > v) swap(a[i], a[r--]);
            else i++;
        }
        sort(a, lo, l-1);
        sort(a, r+1, hi);
    }

public:

    static void sort(vector<T>& a) {
        random_shuffle(a.begin(), a.end());
        sort(a, 0, a.size()-1);
    }

};

int main(int argc, char** argv) {

    vector<int> vec;
    int N = stoi(argv[1]);
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));

    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    Quick3Way<int>::sort(vec);

    cout << "The array after sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    return 0;

}


