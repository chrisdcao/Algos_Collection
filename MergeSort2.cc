#include <iostream>
#include <numeric>
#include <algorithm>
#include <random>

using namespace std;

template <typename T>
class Merge {
private:

    static void merge(vector<T>& orgVec, vector<T>& auxVec, int low, int mid, int high) {
        // precondition: orgVec[low..mid] and orgVec[mid+1..high] are sorted
        for (int k = low; k <= high; ++k) auxVec[k] = orgVec[k];

        int i = low;
        int j = mid+1;
        for (int k = low; k <= high; ++k) {
            // if the elements run out then we change to the other sub-array
            if (i > mid) orgVec[k] = auxVec[j++];
            else if (j > high) orgVec[k] = auxVec[i++];
            // copy the smaller one to orgVec
            else if (aux[i] > aux[j]) orgVec[k] = auxVec[j++];
            else orgVec[k] = auxVec[i++];
        }
    }

    static void sort(vector<T>& orgVec, vector<T>& auxVec, int low, int high) {
        if (high <= low) return;
        sort(orgVec, auxVec, low, mid);
        sort(orgVec, auxVec, mid+1, high);
        merge(orgVec, auxVec, low, mid, high);
    }

public:

    static void sort(vector<T>& orgVec) {
        vector<T> auxVec(orgVec.size());
        sort(orgVec, auxVec, 0, orgVec.size() - 1);
    }

};

// Merge sort
int main(int argc, char** argv) {

    vector<int> arr(stoi(argv[1]));
    iota(arr.begin(), arr.end(), 0);

    cout << "Before sorting: ";
    random_shuffle(arr.begin(),arr.end());
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    Merge<int>::sort(arr);
    // mergesort function
    cout << "After sorting: ";
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    return 0;

}

