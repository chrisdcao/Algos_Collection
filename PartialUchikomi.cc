/* swim from the bottom(k = N) up(till k = 1) */
void swim(int k) {
    while (k > 1 && pq[k/2] < pq[k]) {
        swap(pq[k], pq[k/2]);
        k = k/2;
    }
}

/* when the parent node is smaller than the child node (in a minPQ) */
void sink(int k) {
    while (2 * k < N) {         /* stopping right before the threshold */
        int j = 2 * k;
        /* we go one step down at a time */
        if (j < N && pq[j] < pq[j+1]) j++;
        /* and check if we still need to sink */
        if (!(pq[k] < pq[j])) break;
        /* if yes then swap the place */
        swap(pq[k], pq[j]);
        /* and keep moving forward */
        k = j;
        /* there might be multiple swaps as we have to rearrange the whole branch to find the appropriate spots for all the nodes that was affected*/
    }
}
