#include <iostream>
#include <vector>
#include <memory>
#include <string>
#include <sstream>

using namespace std;

// QUEUE LIKE IMPLEMENTATION, MEANING THAT THE
// OUTPUT WILL DEFAULTLY HAVE THE ORDER OF A QUEUE 
template <typename T>
class DoublyLinkedList  {
private:
    class Node {
    public:
        T item;
        Node* next = NULL;
        Node* prev = NULL;
        int index = -1;
    };

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }
 
    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    vector<unique_ptr<Node>> allNodes;

    Node* head;
    Node* tail;
    int N;

    int isReversed = 0;

public:

    friend ostream& operator << (ostream& os, const DoublyLinkedList& doublylist) {
        auto traverse = doublylist.head;
        if (doublylist.isReversed % 2 == 0) {
            while (traverse != NULL) {
                os << traverse->item << " ";
                traverse = traverse->next;
            }
        } else {
            while (traverse != NULL) {
                os << traverse->item << " ";
                traverse = traverse->prev;
            }
        }

        os << endl;
        return os;
    }

    DoublyLinkedList(): N(0), head(NULL), tail(NULL) {}

    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void insert(T item) {
        Node* temp = newNode();
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            temp->prev = NULL;
            head = temp;
            tail = temp;
        } else {
            temp->prev = tail;
            tail->next = temp;
            tail = temp;
        }
        N++;
    }

    void insertAfter(int index, T item) {
        if (index < 0) return;
        if (index > size() - 1) return;
        Node* traverse;
        Node* temp = newNode();
        temp->item = item;
        if (index > size() / 2) {
            traverse =tail;
            for (int i = 0; i < (size() - 1) - index; i++)
                traverse = traverse->prev;
        } else {
            traverse = head;
            for (int i = 0; i < index; i++)
                traverse = traverse->next;
        }
        temp->next = traverse->next;
        temp->prev = traverse;
        traverse->next->prev = temp;
        traverse->next = temp;
        N++;
    }

    void reverse() {
        Node* oldHead = head;
        head = tail;
        tail = oldHead;
        isReversed++;
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }
};

int main(int argc, char** argv) {
    DoublyLinkedList<string> stringList;
    stringList.insert("phong");
    stringList.insert("thinh");
    stringList.insert("cuong");
    stringList.insert("khanh");

    cout << "Expected outcome: phong thinh cuong khanh" << endl;
    cout << "Real outcome: ";
    cout << stringList;
    cout << endl;

    cout << "Expected outcome: phong thinh cuong khanh" << endl;
    cout << "Real outcome: ";
    cout << stringList.toString();
    cout << endl;

    stringList.reverse();
    cout << "Expected outcome: khanh cuong thinh phong" << endl;
    cout << "Real outcome: ";
    cout << stringList;
    cout << endl;

    return 0;
}

