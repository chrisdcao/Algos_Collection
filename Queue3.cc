#include <iostream>
#include <string>
#include <vector>
#include <memory>
using namespace std;

template <typename T>
class Queue {
private:
    class Node {
    public:
        T item;
        Node* next;
        int index = -1;
    };

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    vector<unique_ptr<Node>> allNodes;

    Node* head;
    Node* tail;
    int N;

    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* initLoc) {
            current = initLoc;
        }

        Iterator& operator++() {
            current = current->next;
            return *this;
        }

        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }

        T operator*() {
            return this->current->item;
        }
    };

public:
    Queue() {
        N = 0;
        head = NULL;
        tail = NULL;
    }
     
    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void enqueue(T item) {
        Node* temp = newNode();
        temp->item = item;
        temp->next = NULL;
        if (N == 0) {
            head = temp;
            tail = temp;
        } else {
            tail->next = temp;
            tail = temp;
        }
        ++N;
    }

    T pop() {
        T item = head->item;
        if (N != 0) {
            Node* dp = head;
            head = head->next;
            free(dp);
            --N;
        } else 
            throw out_of_range("Underflow");
        return item;
    }

    Iterator begin() { return Iterator(head); }
    Iterator end() { return Iterator(tail->next); }
};

int main() {
    Queue<string> stringList;
    stringList.enqueue("khanh");
    stringList.enqueue("thinh");
    stringList.enqueue("cuong");
    stringList.enqueue("phong");
    cout << "Expected: khanh thinh cuong phong" << endl;
    for (auto s : stringList)
        cout << s << " ";
    cout << endl;
}
