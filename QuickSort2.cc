#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include "Random.h"

using namespace std;

template <typename T>
class Quick {
private:

    static void sort(vector<T>& a, int lo, int hi) {
        if (lo >= hi) return;
        int j = partition(a, lo, hi);
        sort(a, lo, j-1);
        sort(a, j+1, hi);
    }

    static int partition(vector<T>& a, int lo, int hi) {
        int i = lo, j = hi + 1;
        T v = a[lo];
        while(true) {
            /* exhaust at both end of the arrays until the elements at both not satisfying
             * only then do we exchange (and will be sure that the exchange help them get into the right place) */
            only then do we swap
            while (a[++i] < v) if (i == hi) break;
            while (v < a[--j]) if (j == lo) break;
            if (i >= j) break;
            swap(a[i], a[j]);
        }
        swap(a[lo], a[j]);
        return j;
    }

public:

    static void sort(vector<T>& a) {
        random_shuffle(a.begin(), a.end());
        sort(a, 0, a.size() - 1);
    }

};

int main(int argc, char** argv) {

    vector<int> vec;
    int N = stoi(argv[1]);
    for (int i = 0;i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));

    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " "; 
    cout << endl;

    Quick<int>::sort(vec);

    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " "; 
    cout << endl;

    return 0;

}
