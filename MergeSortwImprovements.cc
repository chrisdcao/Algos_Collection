#include <iostream>
#include <random>
#include <algorithm>
#include <numeric>
#include <iomanip>
#include <iterator>
#include <vector>

using namespace std;

template <typename T>
class Merge {
private:

    static void InsertionSort(vector<T>& a, int low, int high) {
        int N = high - low + 1;
        for (int i = 1; i < N; i++) {
            for (int j = i; j > 0 && a[j] < a[j-1]; j--) 
                swap(a[j], a[j-1]);
        }
    }

    static void merge(vector<T>& org, vector<T>& aux, int low, int mid, int high) {
        int i = low;
        int j = mid+1;

        for(int k = low; k <= high; ++k) {
            if (i > mid) org[k] = aux[j++];
            else if (j > high) org[k] = aux[i++];
            else if (aux[i] > aux[j]) org[k] = aux[j++];
            else org[k] = aux[i++];
        }
    }

    static void sort(vector<T>& org, vector<T>& aux, int low, int high) {
        if (low >= high) return;
        int mid = low + (high - low) / 2;
        // cutoff
        if (high - low <= 43) InsertionSort(aux, low, high);
        else {
            sort(aux, org, low, mid);
            sort(aux, org, mid+1, high);
        }
        // check is already in order
        if (org[mid] <= org[mid+1]) {
            for (int i = low; i < high; i++)
                org[i] = aux[i];
            return;
        }

        merge(org, aux, low, mid, high);
    }

public:

    static void sort(vector<T>& org) {
        vector<T> aux;
        for (int i = 0; i < org.size(); i++)
            aux.push_back(org[i]);
        sort(org, aux, 0, org.size() - 1);
    }

};

int main(int argc, char** argv) {

    vector<int> vec(stoi(argv[1]));
    iota(vec.begin(), vec.end(), 0);

    random_shuffle(vec.begin(), vec.end());

    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    Merge<int>::sort(vec);

    cout << "The array after sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    return 0;

}
