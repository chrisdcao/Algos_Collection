#include <iostream>
#include <algorithm>
#include <numeric>
#include <random>
#include <vector>

using namespace std;

template <typename T>
class MergeBottomUp {
private:

    static void merge(vector<T>& org, vector<T>& aux, int low, int mid, int high) {
        for (int k = low; k <= high; ++k) aux[k] = org[k];

        int i = low;
        int j = mid+1;
        for (int k = low; k <= high; ++k) {
            if (i > mid) org[k] = aux[j++];
            else if (j > high) org[k] = aux[i++];
            else if (aux[i] > aux[j]) org[k] = aux[j++];
            else org[k] = aux[i++];
        }
    }

public:

    static void sort(vector<T>& org) {
        int N = org.size();
        vector<T> aux(N);
        for (int len = 1; len < N; len *= 2) {
            for (int low = 0; low < N - len; low += len * 2) {
                int mid = low + len - 1;
                // compare between end of the sub array and the end of the whole array
                int high = min(mid + len, N - 1) ;
                merge(org, aux, low, mid, high);
            }
        }
    }
};

// Merge sort
int main(int argc, char** argv) {

    vector<int> arr(stoi(argv[1]));
    iota(arr.begin(), arr.end(), 0);

    cout << "Before sorting: ";
    random_shuffle(arr.begin(),arr.end());
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    MergeBottomUp<int>::sort(arr);
    // mergesort function
    cout << "After sorting: ";
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    return 0;

}
