#pragma once

#include <iostream>
#include <string>
#include <numeric>
#include <sstream>
#include <algorithm>
#include <vector>
#include <array>
#include <climits>
#include <float.h>

using namespace std;

class Edge {
public:
	int from;
	int to;
	double cost=DBL_MAX;
	Edge()=default;
	Edge(int from, int to, double cost): from(from), to(to), cost(cost) {}
	~Edge() {}
};

// adj vertices
class DirectedGraph {
public:
	int V;
	int E;
	vector<vector<int>> adj;

	DirectedGraph(int V): V(V), E(0), adj(V) {}
	~DirectedGraph() {}

	int getV() { return V; }
	int getE() { return E; }

	void addEdge(int from, int to) {
		adj[from].push_back(to);
		E++;
	}

	vector<int>& getAdj(int v) {
		return adj[v];
	}

	string toString() {
		ostringstream oss;
		for (int v=0;v<V;v++) {
			oss << v << ": ";
			for (int& w : adj[v])
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

// adj edges
class EdgeWeightedDirectedGraph {
public:
	int V;
	int E;
	vector<vector<Edge>> adj;

	EdgeWeightedDirectedGraph(int V): V(V), E(0), adj(V) {}
	~EdgeWeightedDirectedGraph() {}

	int getV() { return V; }
	int getE() { return E; }

	void addEdge(int from, int to, double cost) {
		adj[from].push_back(Edge(from,to,cost));
		E++;
	}

	void addEdge(Edge e) {
		adj[e.from].push_back(e);
		E++;
	}

	vector<Edge>& getAdj(int v) {
		return adj[v];
	}

	string toString() {
		ostringstream oss;
		for (int v=0;v<V;v++) {
			oss << v << ": ";
			for (Edge& e : adj[v])
				oss << "(" << e.from << "-" << e.to << ":" << e.cost << "), ";
			oss << endl;
		}
		return oss.str();
	}
};

// adj vertices 
class UndirectedGraph {
public:
	int V;
	int E;
	vector<vector<int>> adj;

	UndirectedGraph(int V): V(V), E(0), adj(V) {}
	~UndirectedGraph() {}

	int getV() { return V; }
	int getE() { return E; }

	void addEdge(int from, int to) {
		adj[from].push_back(to);
		adj[to].push_back(from);
		E++;
	}

	vector<int>& getAdj(int v) {
		return adj[v];
	}

	string toString() {
		ostringstream oss;
		for (int v=0;v<V;v++) {
			oss << v << ": ";
			for (int& w : adj[v])
				oss << w << " ";
			oss << endl;
		}
		return oss.str();
	}
};

// adj vertices 
class EdgeWeightedUndirectedGraph {
public:
	int V;
	int E;
	vector<vector<Edge>> adj;

	EdgeWeightedUndirectedGraph(int V): V(V), E(0), adj(V) {}
	~EdgeWeightedUndirectedGraph() {}

	int getV() { return V; }
	int getE() { return E; }

	void addEdge(int from, int to, double cost) {
		adj[from].push_back(Edge(from,to,cost));
		adj[to].push_back(Edge(to,from,cost));
		E++;
	}

	vector<Edge>& getAdj(int v) {
		return adj[v];
	}

	string toString() {
		ostringstream oss;
		for (int v=0;v<V;v++) {
			oss << v << ": ";
			for (Edge& e : adj[v])
				oss << "(" << e.from << "-" << e.to << ":" << e.cost << "), ";
			oss << endl;
		}
		return oss.str();
	}
};

#define EWD EdgeWeightedDirectedGraph

class EdgeWeightedDirectedCycle {
public:
	vector<bool> marked;
	vector<Edge*> edgeTo;
	vector<bool> onStack;
	vector<Edge> cycle;

	EdgeWeightedDirectedCycle(EWD& G): marked(G.V), edgeTo(G.V), onStack(G.V) {
		for (int v=0;v<G.V;v++) 
			dfs(G,v);
	}
	~EdgeWeightedDirectedCycle() {}

	void dfs(EWD& G, int v) {
		onStack[v]=true;
		marked[v]=true;
		for (auto& e : G.adj[v]) {
			int w =e.to;
			if (hasCycle()) return;
			if (!marked[w]) {
				edgeTo[w]=&e;
				dfs(G,w);
			}
			else if (onStack[w])
				recordCycle(e,w);
		}
		onStack[v]=false;
	}

	inline bool hasCycle() { return !cycle.empty(); }

	void recordCycle(Edge e,int w) {
		for (Edge x=e; x.from!=w; x=*edgeTo[x.from])
			cycle.push_back(x);
	}

	inline vector<Edge>& getCycle() { return cycle; }
};
