GXX=g++
FXX=-Wall -Wextra

BellmanFordSP: BellmanFordSP.cc
	$(GXX) $(FXX) -o BellmanFordSP BellmanFordSP.cc

BellmanFordSP2: BellmanFordSP2.cc
	$(GXX) $(FXX) -o BellmanFordSP2 BellmanFordSP2.cc

BellmanFordSP3: BellmanFordSP3.cc
	$(GXX) $(FXX) -o BellmanFordSP3 BellmanFordSP3.cc

BellmanFordSP4: BellmanFordSP4.cc
	$(GXX) $(FXX) -o BellmanFordSP4 BellmanFordSP4.cc

BellmanFordSP5: BellmanFordSP5.cc
	$(GXX) $(FXX) -o BellmanFordSP5 BellmanFordSP5.cc
