#include <iostream>
using namespace std;

int Fibonacci(int n) {
    if (n <= 1) return n;
    else return Fibonacci(n - 1) + Fibonacci(n - 2);
}

int main() {
    int n;
    cout << "Please input the length of Fib sequence: ";
    cin >> n;
    cout << "The output array is: ";
    for (int i = 1; i < n; i++) {
        cout << Fibonacci(i) << " ";
    }
    cout << endl;
    return 0;
}
