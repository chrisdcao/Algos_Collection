#include <iostream>
#include <algorithm>
#include <random>
#include <vector>
#include "Random.h"

using namespace std;

template <typename T>
class Quick3Way {
private:

    static void sort(vector<T>& a, int lo, int hi) {
        if (lo >= hi) return;
        int lt = lo, i = lo + 1, gt = hi;
        T v = a[lo];
        /* we keep traversing until current pointer meets the right-side pointer */
        while (i <= gt) {
            /* if it is smaller then we swap it onto the left side of the array (swapping it with the leftside - pointer)
             * by keep swapping the pivot with the value like this during our comparing process, we don't have to do one last swap for the pivot to be in place. This already move the pivot into the right place */
            if (a[i] < v) swap(a[i++], a[lt++]);
            /* if it's larger then we swap with the right-side pointer so that it sits to the right ofo the array */
            else if (a[i] > v) swap(a[i], a[gt--]);
            else i++;
        }
        /* this one is because after the condition checking the 2 pointers are at one step before (with the pointer starting from right) and one step after (with the pointer starting from left). Thus we have to do this to retrieve them back to right place */
        sort(a, lo, lt-1);
        sort(a, gt+1, hi);
    }

public:

    static void sort(vector<T>& a) {
        random_shuffle(a.begin(), a.end());
        sort(a, 0, a.size()-1);
    }

};

int main(int argc, char** argv) {

    vector<int> vec;
    int N = stoi(argv[1]);
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));

    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    Quick3Way<int>::sort(vec);

    cout << "The array after sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;

    return 0;

}

