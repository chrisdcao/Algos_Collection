#include <iostream>

using namespace std;

int Fibonacci(int N) {
    if (N == 0) return 0;
    if (N == 1) return 1;
    if (N == 2) return 1;
    int sum = 0;
    int sum1 = 1;
    int sum2 = 1;
    for (int i = 2; i < N; i++) {
        sum = sum1 + sum2;
        sum2 = sum1;
        sum1 = sum;
    }
    return sum;
}

int main() {
    cout << "Please input the length of Fibonacci array: ";

    int N;
    cin >> N;

    for (int i = 1; i <= N; i++)
        cout << Fibonacci(i) << endl;

    return 0;
}
