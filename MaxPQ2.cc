#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric> 
#include "Stack.h"
#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

template <typename T>
class MaxPQ {
private:

    vector<T> pq;
    int N = 0;

    void swim(int k) {
        while (k > 1 && pq[k/2] < pq[k]) {
            swap(pq[k/2], pq[k]);
            k = k/2;
        }
    }

    void sink(int k) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j] < pq[j+1]) j++;
            if (pq[k] >= pq[j]) break;
            swap(pq[k], pq[j]);
            k = j;
        }
    }

public:

    friend ostream& operator<<(ostream& os, MaxPQ<T> maxPQ) {
        while (!maxPQ.isEmpty())
            os << maxPQ.delMax() << " ";
        return os;
    }

    MaxPQ() = default;

    MaxPQ(int maxN): pq(maxN + 1) { }

    virtual ~MaxPQ() {  }
    
    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void insert(T item) { 
        pq[++N] = item;
        swim(N);
    }

    T getMax() { return pq[1]; }

    T delMax() {
        if (N == 0) {
            cout << "underflow. Returning default item value: ";
            return T();
        }
        T item = pq[1];
        swap(pq[1], pq[N--]);
        pq[N+1] = NULL;
        sink(1);
        return item;
    }

};

int main(int argc, char** argv) {

    int M = stoi(argv[1]);
    MaxPQ<int> pq(M + 1);
    int x;
    while (cin >> x) {
        pq.insert(x);
        if (pq.size() > M)
            pq.delMax();    
    }
    Stack<int> stack;
    while (!pq.isEmpty()) stack.push(pq.delMax());
    for (auto t : stack) cout << t << endl;

    return 0;

}

