#include <iostream>
#include <vector>
#include "Random.h"

using namespace std;

template <typename T>
class Selection {
public:
    static void sort(vector<T>& a) {
        int N = a.size();
        for (int i = 0; i < N; i++) {
            int iMin = i;
            for (int j = i+1; j < N; j++)
                if (a[j] < a[iMin])
                    iMin = j;
            swap(a[i], a[iMin]);
        }
    }
};

int main(int argc, char** argv) {
    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0,N-1));
    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;
    Selection<int>::sort(vec);
    cout << "\nThe array after sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;
}
