#include <iostream>
#include "Random.h"
#include "ContainerUtility.h"

using namespace std;

/* MaxHeap : parent > child */
template <typename T>
class MaxMechanismHeap {
private:

    /* -1 from k and j because vector starts from index 0 instead of 1 like PQ */
    static void sink(vector<T>& pq, int k, int N) {
        while (2 * k <= N) {
            int j = 2 * k;
            /* if (not satisfied) traverse fwd; */
            if (j < N && pq[j-1] < pq[j]) j++;
            if (pq[k-1] >= pq[j-1]) break;
            swap(pq[k-1], pq[j-1]);
            k = j;
        }
    }

public:

    static void sort(vector<T>& a) {
        /* because the last index in a vector is size-1, not size (because the first index marked from '0', unlike Heap)
         * thus we use a.size() - 1 as our N */
        int N = a.size()-1;
        for (int k = N/2; k >= 1; k--)
            sink(a, k, N);
        while (N >= 1) {
            swap(a[0], a[N]);
            /* sink from '1' is sink from '0' because in our sink() function we have already decrease by '1'
             * we cannot use '0' straight because '0' cannot be multiplied by any numbers to go 'log' step */
            sink(a, 1, N);
            N--;
        }
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0, N-1));
    cout << "The array before sorting: ";
    cout << vec << endl;

    MaxMechanismHeap<int>::sort(vec);

    cout << "The array after sorting: ";
    cout << vec << endl;

}
