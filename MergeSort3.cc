#include <iostream>
#include <iomanip>
#include <algorithm>
#include <random>
#include <vector>

using namespace std;

template <typename T>
class Merge {
private:

    static void merge(vector<T>& orgVec, vector<T>& auxVec, int low, int mid, int high) {
        for(int k = low; k <= high; ++k) auxVec[k] = orgVec[k];
        
        int i = low;
        int j = mid+1;
        int arrayIndex = low;

        //while (i <= mid) {
            // if a normal merge-sort try to do what Faster merge do (ex_02_02_11), then j will have no limit and can go forever to the right of the aux[], which are defaultly assigned to 0 and always make 'j' condition valid
            // and thus will replace all the real elements of the array with '0' defaultly init value
            //cout << auxVec[i] << " " << auxVec[j] << endl;
            //if (auxVec[i] <= auxVec[j]) orgVec[arrayIndex++] = auxVec[i++];
            //else                       orgVec[arrayIndex++] = auxVec[j++];
        //}

        for (int k = low; k <= high; ++k) {
            if (i > mid) orgVec[k] = auxVec[j++];
            else if (j > high) orgVec[k] = auxVec[i++];
            else if (auxVec[i] > auxVec[j]) orgVec[k] = auxVec[j++];
            else orgVec[k] = auxVec[i++];
        }
    }

    static void sort(vector<T>& orgVec, vector<T>& auxVec, int low, int high)  {
        if (high <= low)  return;
        int mid = low + (high - low) / 2;
        sort(orgVec, auxVec, low, mid);
        sort(orgVec, auxVec, mid+1, high);
        merge(orgVec, auxVec, low, mid, high);
    }

public:

    static void sort(vector<T>& orgVec) {
        vector<T> auxVec(orgVec.size());
        sort(orgVec, auxVec, 0, orgVec.size() - 1);
    }

};

// Merge sort
int main(int argc, char** argv) {

    vector<int> arr(stoi(argv[1]));
    iota(arr.begin(), arr.end(), 0);

    cout << "Before sorting: ";
    random_shuffle(arr.begin(),arr.end());
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    Merge<int>::sort(arr);
    // mergesort function
    cout << "After sorting: ";
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    return 0;

}

