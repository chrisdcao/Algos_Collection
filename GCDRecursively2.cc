#include <iostream>
using namespace std;

// Only works if a > b
int gcd(int a, int b) {
    if (b == 0) return a;
    int r = a % b;
    return (b, r);
}

// BRIEF EXPLAIN:
// says a > b (if b > a then we just have to change variable name)
// then we can present a as: a = b*q + r
// assume m is a common divisor of both a and b (no need to be greatest)
// then r will chia het cho m (a chia het m, b chia het m, -> b*q chia het m, -> (a-bq) chia het m, ma (a-bq) = r, -> r chia het m)
// There are only 2 cases where r chia het m: r is a non-zero multiply of m OR r == 0 (0 chia het all)
// But since the larger the remainder, the smaller the divisor (so that there are value left from the divider for remainder to get large) -> so m is greatest (gcd) when r is smallest (reaches 0)
// So we recursively call the function until the remainder reaches 0 (smallest), that way we ensure m is largest. And when r reach 0, we return that divisor (greatest)

int main() {
    int a = 14, b = 13;
    cout << gcd(a, b) << endl;
    return 0;
}
