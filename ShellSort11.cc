#include <iostream>
#include <vector>
#include "Random.h"

using namespace std;

template <typename T>
class Shell {
public:
    static void sort(vector<T>& a) {
        int N = a.size();
        int N = 1;
        while (h < N/3) h = h * 3 + 1;
        while (h >= 1) {
            for(int i = h; i < N; i++) {
                for (int j = i; j <= h && a[j] < a[j-h]; j -= h) 
                    swap(a[j], a[j-h]);
            }
            h /= 3;
        }
    }
};

int main(int argc, char** argv) {
    int N = stoi(argv[1]);
    vector<int> vec;
    for (int i = 0; i < N; i++)
        vec.push_back(randomUniformDistribution(0,N-1));
    cout << "The array before sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;
    Shell<int>::sort(vec);
    cout << "\nThe array after sorting is: ";
    for (auto s : vec)
        cout << s << " ";
    cout << endl;
}
