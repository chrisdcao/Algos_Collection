#include "SinglyLinkedListQueue.h"
#include "Random.h"

using namespace std;

#define List SinglyLinkedList

/* RESEARCH: 1. about the asterisk that's before pointer (double pointer) and what they means
 * RESEARCH 2: ersearch about the changing of the value of the references when being passed to the function
 * and how it impacts to the result of the recursion */
template <typename T>
class Merge {
    using NodePtr = SinglyLinkedList<int>::Node*;
private:
    
    /* static NodePtr SortedMerge(NodePtr, NodePtr);
     * static void FrontBackSplit(NodePtr, NodePtr*, NodePtr*); */

    /* we have a pointer pointing to pointer of headRef
     * a pointer that hold the address to the address of an object
     * which is another address? */

    /* this pointer will have the address of x-bit after the original pointer (with x depends no how many the original type of the  variable cost (i.e for int cost 4-bit or string 8-bit)) */
    static void MergeSort(NodePtr* headRef) {
        /* head  is containing the pointer to headRef (because we have dereferenced the double pointer */
        NodePtr head = *headRef, 
                a, b;

        /* if the head == NULL or head->next == NULL then stop */
        if ((head == NULL) || (head->next == NULL)) return;

        /* by doing frontbacksplit passing references to a and b
         * we are assignnig the value of each head of each part to hte 2 vars */
        FrontBackSplit(head, &a, &b);

        /* a is the head1 b is head2 
         * this recursively call the function with the length being divided in half */
        MergeSort(&a);
        MergeSort(&b);

        /* chúng ta để giá pointer to headref = kết quả rsorrted merge của a và b
         * headref will be the head of the small-sorted array */
        /* and we have a pointer to the sub-sorted array head */
        *headRef = SortedMerge(a, b);
    }

    /* the MergeSort above is the one merging larger chunks 
     * this keep merge-sorting 2 items rather than the larger chunks of the array */
    static NodePtr SortedMerge(NodePtr a, NodePtr b) {
        NodePtr result = NULL;
        /* if either Node is NULL then we return the other node */
        if (a == NULL) return b;
        else if (b == NULL) return a;

        /* this one is to have the smallest element to be the result
         * so result will return the head of each smaller chunks */
        if (a->item <= b->item) {
            result = a;
            /* we link the next to the first of the after chunks. This one has the same thoughts that I did in the first version of ex17 */
            result->next = SortedMerge(a->next, b);
        } else {
            result = b;
            /* similar idea */
            result->next = SortedMerge(a, b->next);
        }
        /* in the end return the first of the first chunk in the small-sorted array */
        return result;
    }

    /* this obtain the front and back portion of the array
     * this frontback split is to help the recursion of the first function to keep dividing the array into halves of the current portion */
    static void FrontBackSplit(NodePtr source, NodePtr* frontRef, NodePtr* backRef) {
        NodePtr fast;
        NodePtr slow;
        /* fast is one node after slow so that slow is always <= N/2 (one always stop at the lower portion of the middle) */
        slow = source;
        fast = source->next;

        while (fast != NULL) {
            fast = fast->next;
            if (fast != NULL) {
                slow = slow->next;
                /* by also incrementing 'fast' in this block (2x increment of fast)
                 * fast is travelling 2x as fast as slow
                 * -> when fast travel N steps then slow travel N/2 steps (and we can extract middle) */
                fast = fast->next;
            }
        }

        /* slow is before the midpoint in the list */
        *frontRef = source;
        *backRef = slow->next;
        /* we actually split the list into 2 smaller list */
        slow->next = NULL;
    }

public:

    static void sort(List<T>& org) {
        NodePtr* headRef = &org.head;
        MergeSort(headRef);
    }

};

int main(int argc, char** argv) {

    int N = stoi(argv[1]);
    List<int> intList;
    for (int i = 0; i < N; i++)
        intList.insert(randomUniformDistribution(0, N-1));
    cout << "The array before sorting is: ";
    cout << intList << endl;

    Merge<int>::sort(intList);
    cout << "The array after sorting is: ";
    cout << intList << endl;

    return 0;

}

