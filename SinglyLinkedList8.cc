#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

using namespace std;

template <typename T>
class SinglyLinkedList {
private:
    class Node {
    public:
        T item;
        Node* next = NULL;
        int index = -1;
    };

    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    vector<unique_ptr<Node>> allNodes;

    int N;
    Node* head;

public:
    SinglyLinkedList(): N(0), head(NULL) {}

    friend ostream& operator << (ostream& os, SinglyLinkedList& list) {
        auto traverse = list.head;
        while (traverse != NULL) {
            os << traverse->item << " ";
            traverse = traverse->next;
        }
        os << endl;
        return os;
    }

    void insert(T item) {
        Node* temp = newNode();
        temp->item = item;
        if (N == 0) 
            temp->next = NULL;
        else 
            temp->next = head;
        head = temp;
        N++;
    }

    void insertAfter(int index, T item) {
        if (index < 0 || index > N - 1)
            cout << "Index inappropriate" << endl;
        else {
            if (index == N - 1) insert(item);
            else {
                Node* traverse = head;
                for (int i = 0; i < index - 1; i++)
                    traverse = traverse->next;
                Node* temp = newNode();
                temp->item = item;
                temp->next = traverse->next;
                traverse->next = temp;
                N++;
            }
        }
    }

    void removeAtBegin() {
        if (head == NULL) return;
        else {
            Node* oldHead = head;
            head = head->next;
            freeNode(oldHead);
            N--;
        }
    }

    Node* reverseIteratively(Node* head) {
        if (N <= 1) return head;
        else {
            Node* prev = NULL;
            Node* current = head;
            Node* next = head->next;
            while (next != NULL) {
                current->next = prev;
                prev = current;
                current = next;
                next = next->next;
            }
            current->next = prev;
            return current;
        }
    }

    Node* reverseRecursively(Node* head) {
        if (head == NULL) return NULL;
        if (head->next == NULL) return head;
        Node* second = head->next;
        Node* newHead = reverseRecursively(second);
        second->next = head;
        head->next = NULL;
        return newHead;
    }

    void reverse(bool isReversed = true) {
        if (isReversed) head = reverseRecursively(head);
        else  head = reverseIteratively(head);
    }

    string toString() {
        ostringstream oss;
        oss << *this;
        return oss.str();
    }
};

int main(int argc, char** argv) {
    SinglyLinkedList<string> stringList;
    stringList.insert("phong");
    stringList.insert("thinh");
    stringList.insert("cuong");
    stringList.insert("khanh");

    cout << "Expected outcome: khanh cuong thinh phong" << endl;
    cout << "Real outcome: ";
    cout << stringList;
    cout << endl;

    cout << "Expected outcome: khanh cuong thinh phong" << endl;
    cout << "Real outcome: ";
    cout << stringList.toString();
    cout << endl;

    stringList.reverse();
    cout << "Expected outcome: phong thinh cuong khanh" << endl;
    cout << "Real outcome: ";
    cout << stringList;
    cout << endl;

    stringList.reverse(false);
    cout << "Expected outcome: khanh cuong thinh phong" << endl;
    cout << "Real outcome: ";
    cout << stringList;
    cout << endl;

    return 0;
}

