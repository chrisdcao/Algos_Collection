#include <iostream>
#include <vector>
#include <random>
#include "Random.h"
#include "ContainerUtility.h"
#include "Stack.h"

using namespace std;

/* MaxHeap: Parent > child */
template <typename T>
class MaxPQ {
private:

    vector<T> pq;
    int N = 0;

    void sink(int k) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && pq[j] < pq[j+1]) j++;
            if (pq[k] >= pq[j]) break;
            swap(pq[k], pq[j]);
            k = j;
        }
    }

    void swim(int k) {
        while (k > 1 && pq[k/2] < pq[k]) {
            swap(pq[k], pq[k/2]);
            k = k/2;
        }
    }

public:

    friend ostream& operator << (ostream& os, MaxPQ<T> maxPQ) {
        while (!maxPQ.isEmpty())
            os << maxPQ.delMax() << " ";
        return os;
    }

    MaxPQ() = default;

    MaxPQ(int maxN): pq(maxN+1) {}

    virtual ~MaxPQ() {}

    int size() { return N; }

    bool isEmpty() { return N == 0; }

    void insert(T item) {
        pq[++N] = item;
        swim(N);
    }

    T getMax() { return pq[1]; }

    T delMax() {
        if (N == 0) {
            cout << "Underflow. Returning default value of type: ";
            return T();
        }
        T item = pq[1];
        /* swap to delete */
        swap(pq[1], pq[N--]);
        /* delete (the item, not the space) */
        pq[N+1] = NULL;
        /* restore the Heap structure, since we just swap the smallest element up */
        sink(1);
        return item;
    }

};

int main(int argc, char** argv) {

    int M = stoi(argv[1]);
    MaxPQ<int> pq(M + 1);
    int x;
    while (cin >> x) {
        pq.insert(x);
        if (pq.size() > M)
            pq.delMax();    
    }
    Stack<int> stack;
    while (!pq.isEmpty()) stack.push(pq.delMax());
    for (auto t : stack) cout << t << endl;

    return 0;

}
