#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

template <typename T>
class ExponentialSearch {
private:

    static int BinarySearch(int key, int lo, int hi, vector<int>& vec) {
        while (lo <= hi) {
            int mid = lo  + (hi - lo) / 2;
            if (key > vec[mid]) lo = mid + 1;
            else if (key < vec[mid]) hi = mid - 1;
            else return mid;
        }
        return -1;
    }

public:

    static int indexOf(int key, vector<int>& vec) {
        if (vec[0] == key) return 0;

        int i = 1;

        while (vec[i] < key && i < vec.size()) i *= 2;

        int lo = i / 2;
        int hi = min(i, (int) vec.size() - 1);

        return BinarySearch(key, lo, hi, vec);
    }
};

int main(int argc, char** argv) {

    vector<int> vec = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    cout << ExponentialSearch<int>::indexOf(7,vec) << endl;
    cout << ExponentialSearch<int>::indexOf(4,vec) << endl;

    return 0;
}
