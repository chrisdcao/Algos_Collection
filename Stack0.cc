#include <iostream>
#include <vector>
#include <memory>
#include <string>
using namespace std;

template <typename T>
class Stack {
private:

    class Node {
    public:
        T item;
        Node* next;
        int index = -1;
    };

    Node* newNode() {
        allNodes.push_back(make_unique<Node>());
        allNodes.back()->index = allNodes.size() - 1;
        return allNodes.back().get();
    }

    void freeNode(Node* node) {
        if (node->index < allNodes.size() - 1) {
            allNodes[node->index].swap(allNodes.back());
            allNodes[node->index]->index = node->index;
        }
        allNodes.pop_back();
    }

    class Iterator {
    private:
        Node* current;
    public:
        Iterator(Node* initLoc) {
            current = initLoc;
        }

        Iterator& operator++() {
            current = current->next;
            return *this;
        }

        bool operator!=(Iterator& rhs) {
            return this->current != rhs.current;
        }

        T operator*() {
            return this->current->item;
        }
    };

    vector<unique_ptr<Node>> allNodes;
    Node* head;
    Node* tail;
    int N;
    
public:
    Stack() {
        N = 0;
        head = NULL;
        tail = NULL;
    }
    
    int size() { return N; }
    bool isEmpty() { return N == 0; }

    void push(T item) {
        Node* temp = newNode();
        temp->item = item;
        if (N == 0) {
            temp->next = NULL;
            head = temp;
            tail = temp;
        } else {
            temp->next = head;
            head = temp;
        }
        ++N;
    }

    T pop() {
        T item = head->item;
        if (N != 0) {
            Node* dp = head;
            head = head->next;
            freeNode(dp);
            --N;
        } else 
            throw out_of_range("Underflow");
        return item;
    }

    Iterator begin() { return Iterator(head); }
    Iterator end() { return Iterator(tail->next); }
};

int main() {
    Stack<string> stringList;
    stringList.push("khanh");
    stringList.push("thinh");
    stringList.push("cuong");
    stringList.push("phong");
    cout << "Expected: phong cuong thinh khanh" << endl;
    for (auto s : stringList)
        cout << s << " ";
    cout << endl;
}
