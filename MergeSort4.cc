#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>

using namespace std;

template <typename T>
class Merge {
private:

    static void merge(vector<T>& org, vector<T>& aux, int low, int mid, int high) {
        for (int k = low; k <= high; ++k) aux[k] = org[k];
        
        int i = low;
        int j = mid+1;
        for (int k = low; k <= high; ++k) {
            if (i > mid) org[k] = aux[j++];
            else if (j > high) org[k] = aux[i++];
            else if (aux[i] > aux[j]) org[k] = aux[j++];
            else org[k] = aux[i++];
        }
    }

    static void sort(vector<T>& org, vector<T>& aux, int low, int high) {
        if (high <= low) return;
        sort(org, aux, low, mid);
        sort(org, aux, mid+1, high);
        merge(org, aux, low, mid, high);
    }

public:

    static void sort(vector<T>& org) {
        vector<T> aux(org.size());
        sort(org, aux, 0, org.size()-1);
    }
    
};

// Merge sort
int main(int argc, char** argv) {

    vector<int> arr(stoi(argv[1]));
    iota(arr.begin(), arr.end(), 0);

    cout << "Before sorting: ";
    random_shuffle(arr.begin(),arr.end());
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    Merge<int>::sort(arr);
    // mergesort function
    cout << "After sorting: ";
    for (auto s : arr)
        cout << s << " ";
    cout <<  endl;

    return 0;

}
